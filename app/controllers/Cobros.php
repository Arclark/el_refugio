<?php
ob_start();
session_start();

if (!isset($_SESSION['nombre'])) {
  redireccionar('login');
}else{

    if ($_SESSION['permiso']=="administrador") {
    
    
class Cobros extends Controlador
{   
    function __construct(){
        $this->cobroModelo=$this->modelo('Cobro');
    }

    public function index(){

       
        $this->vista('cobros/cobros_vista');
    }

    public function mostrar_usuario(){
        if ($_SERVER['REQUEST_METHOD']=='POST') {

            $datos_usuarios= $this->cobroModelo->mostrar_usuario();

            echo "<option value='' selected=''>--Seleccion un usuario para realizar el cobro--</option>";
            foreach ($datos_usuarios as $usuarios) {
                # code...
                echo "<option value=" .Encriptacion::encryption($usuarios->idusuario) . ">" . $usuarios->nombre . "</option>";
 
          } 

        }else{

            redireccionar(cobros);
        }



    }
    public function mostrar_total(){
        if ($_SERVER['REQUEST_METHOD']=='POST') {

            $codigo=Encriptacion::decryption($_POST['codigo']);
            $deuda= $this->cobroModelo->mostrar_total_deuda($codigo);

            echo '$'. number_format((float)round($deuda->total,2),2,'.','');
            

        }else{

            redireccionar(cobros);
        }

    }

    public function generar_cobro(){
        if ($_SERVER['REQUEST_METHOD']=='POST') {

           // $pago=Encriptacion::decryption($_POST['pago']);
                $suma=0;
            

            if(isset($_POST['pago'])){
                $pago=$_POST['pago'];
            for ($i=0; $i < count($pago); $i++) { 
                if(!empty($pago[$i])){

                $deuda= $this->cobroModelo->mostrar_pagos(Encriptacion::decryption($pago[$i]));
                $suma+=$deuda->total_pagar;
                }
                
            }
            /*foreach ($pago as $pagos) {
                $deuda= $this->cobroModelo->mostrar_pagos(Encriptacion::decryption($pagos));

                $suma+=$deuda->total_pagar;
            }
           */

           echo number_format((float)round($suma,3),3,'.','');
        }else{
            echo 0;
        }
            //echo var_dump($pago);
        }else{

            redireccionar(cobros);
        }

    }
    public function listar($usuario=null){

        if ($_SERVER['REQUEST_METHOD']=='POST' and $usuario!=null) {
            # code...
            $usuario_cod=Encriptacion::decryption($usuario);
            $datos_cobros= $this->cobroModelo->listar($usuario_cod);

            $datos= array();

            
            foreach ($datos_cobros as $cobros) {
             $datos[]=array(//! guardamos los datos en el array creado con la siguiente estructura
                 "0"=>Encriptacion::encryption($cobros->idcobro), //! indece 0 para el codigo
     
                 //! en el indice 2 guarda el nombre del registro
                 "1"=>$cobros->servicio,
                
                 "2"=>'$'. number_format((float)round($cobros->total_pagar,2),2,'.','') ,
                 "3"=>$cobros->mes,
                 "4"=>$cobros->fecha,
                 "5"=>$cobros->tipo_pago,
                 "6"=>"<input type='checkbox' class='form-check-input' name='pago' id='pago' value=".Encriptacion::encryption($cobros->idcobro).">"
                
              );
            }
        

        
            $resultado=array( //! array con la informacion para el DATATABLE DE JQUERY
                
             "sEcho"=>1, //!Informacion para el datatables
             "iTotalRecords"=>count($datos),//!Total de registros para el datatable
             "iTotalDisplayRecords"=>count($datos),//!Datos totales a visualizar
             "aaData"=>$datos //! los datos para el datatable seria el array de la consulta ya con el formato necesario
          );
     
            echo json_encode($resultado);
        }else {
            
           redireccionar(cobros);
        }
        
    }

    public function cobrar(){
        if ($_SERVER['REQUEST_METHOD']=='POST') {

            $contC=0;
            $contDC=0;
            $usuario=Encriptacion::decryption($_POST['usuario']);
            $efectivo=$_POST['efectivo'];
            $total=$_POST['total'];
            $cambio=$_POST['cambio']==0?'0.00':$_POST['cambio'];

            $transaccion= $this->cobroModelo->insert_transaccion($usuario,$efectivo,$total,$cambio);

            if(isset($_POST['pago'])){
                $pago=$_POST['pago'];
            for ($i=0; $i < count($pago); $i++) { 
                if(!empty($pago[$i])){

            $cobro= $this->cobroModelo->cobrar(Encriptacion::decryption($pago[$i]));
            $detalle_trans= $this->cobroModelo->detalle_transaccion($transaccion,Encriptacion::decryption($pago[$i]));
          $contC+=$cobro;
           $contDC+=$detalle_trans;
            }
                
            }
        

          
                echo "Cobro Realizado con exito";
          
        }
            

        }else{

            redireccionar(cobros);
        }

    }

    
}

}//fin ifpermiso
else{
    redireccionar();
}
}//fin if session nombre

ob_end_flush();
