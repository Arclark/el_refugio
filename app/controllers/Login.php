<?php
ob_start();
session_start();
Class Login extends Controlador{

function __construct(){

    $this->loginModelo=$this->modelo('Iniciar_sesion');
}

public function index(){

    if (isset($_SESSION['nombre'])) {
        redireccionar();
    }else{
    $this->vista('login/login_vista');
    }
}

public function verificar(){

    

    if ($_SERVER['REQUEST_METHOD']=="POST") {
            
        $correo=$_POST['correo'];
        $clave=$_POST['pass'];

        $clavehash=hash("SHA256",$clave.SALT);
       
        $user= $this->loginModelo->verificar_usuario($correo,$clavehash);

        if(!empty($user)){
        $name=explode(" ",$user->nombre);
        $lastname=explode(" ",$user->apellido);
        
        $nombre_user=$name[0]." ".$lastname[0];

        $_SESSION['idusuario']=Encriptacion::encryption($user->idusuario);
        $_SESSION['nombre']=$nombre_user;
        $_SESSION['permiso']=$user->permiso;

    echo 1;

}else{
  
echo 0;
}


}else {
    redireccionar(login);
}


}


public function salir(){
    //Limpiamos las variables de sesión   
   session_unset();
   //Destruìmos la sesión
  session_destroy();
   
redireccionar('login');
}


}

ob_end_flush();