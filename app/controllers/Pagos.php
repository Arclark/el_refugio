<?php

ob_start();
session_start();

if (!isset($_SESSION['nombre'])) {
  redireccionar('login');
}else{

    if ($_SESSION['permiso']=="administrador") {
    
    
Class Pagos extends Controlador{

    function __construct(){

        $this->pagoModelo=$this->modelo('Pago');
    }

    public function index(){

       // echo date('Y-m-d');
        $this->vista('pagos/pagos_vista');
    }

    public function insert(){
        if ($_SERVER['REQUEST_METHOD']=='POST') {
            $mes=$_POST['mes'];
            $servicio=Encriptacion::decryption($_POST['servicio']);
            $total_pagar=$_POST['total_pagar'];
            $tarifa=isset($_POST['tarifa'])?$_POST['tarifa']:"";

            $mensaje=$this->pagoModelo->insert($servicio,$total_pagar,$mes,$tarifa);

            echo $mensaje==1?"Pago regístrado con éxito":$mensaje;


        }else{

            redireccionar(pagos);
        }
    }

    public function anular(){
        if ($_SERVER['REQUEST_METHOD']=='POST') {
           $codigo=Encriptacion::decryption($_POST['codigo']);

            $mensaje=$this->pagoModelo->anular($codigo);

            echo $mensaje==1?"Pago Anulado con éxito":$mensaje;


        }else{

            redireccionar(pagos);
        }
    }
    public function mostrar_servicios(){
        if ($_SERVER['REQUEST_METHOD']=='POST') {

            $datos_servicios= $this->pagoModelo->mostrar_servicios();

            echo "<option value='' selected=''>--Opciones--</option>";
            foreach ($datos_servicios as $servicios) {
                # code...
                echo "<option value=" .Encriptacion::encryption($servicios->idservicio) . ">" . $servicios->nombre . "</option>";
 
          } 

        }else{

            redireccionar(pagos);
        }

    }


    public function listar(){

        if ($_SERVER['REQUEST_METHOD']=='POST') {
            # code...
            $datos_pagos= $this->pagoModelo->listar();

            $datos= array();

        
            foreach ($datos_pagos as $pagos) {
             $datos[]=array(//! guardamos los datos en el array creado con la siguiente estructura
                 "0"=>Encriptacion::encryption($pagos->idpago), //! indece 0 para el codigo
     
                 //! en el indice 1 se crean un condicional de una linea para crear los botones de Editar|Desactivar o Editar|Activar dependiendo de la condicion de los registros
                 "1"=>"<button class='btn btn-danger' title='Anular Pago' onclick=anular('".Encriptacion::encryption($pagos->idpago)."')>Anular <i class='fa fa-close'></i></button>",
                 //! en el indice 2 guarda el nombre del registro
                 "2"=>$pagos->servicio,
                 "3"=>'$'. number_format((float)round($pagos->total_pagar,2),2,'.','') ,
                 "4"=>$pagos->mes,
                 "5"=>$pagos->fecha,
                 "6"=>$pagos->tarifa_admin>0?$pagos->tarifa_admin:"N/A",
                 //! en el indice 3 elvaluamo la condicion del registro si es activo osea 1 mostrara un label verde u la leyenta activo
                 //! si la condicion es inactivo osea 0 mostrara un label rojo con la leyenda inactivo
                 "7"=>$pagos->condicion?"<span class='label bg-green'>Activo</span>":
                 ""
                
              );
            }
          
            $resultado=array( //! array con la informacion para el DATATABLE DE JQUERY
                
             "sEcho"=>1, //!Informacion para el datatables
             "iTotalRecords"=>count($datos),//!Total de registros para el datatable
             "iTotalDisplayRecords"=>count($datos),//!Datos totales a visualizar
             "aaData"=>$datos //! los datos para el datatable seria el array de la consulta ya con el formato necesario
          );
     
            echo json_encode($resultado);
        }else {
            
            redireccionar(pagos);
        }
        
    }




    //PARA CLIENTE

    public function pagos_anulados(){

        // echo date('Y-m-d');
         $this->vista('pagos/pagos_anulados_vista');
     }

}

}//fin ifpermiso
else{
    redireccionar();
}
}//fin if session nombre

ob_end_flush();