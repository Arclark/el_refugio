<?php

ob_start();
session_start();

if (!isset($_SESSION['nombre'])) {
  redireccionar('login');
}else{

    if ($_SESSION['permiso']=="usuario") {
    
    
Class Pagos_anulados extends Controlador{

    function __construct(){

        $this->pagoModelo=$this->modelo('Pago');
    }

    public function index(){

       // echo date('Y-m-d');
        $this->vista('pagos/pagos_anulados_vista');
    }

    
    public function listar(){

      if ($_SERVER['REQUEST_METHOD']=='POST') {
            # code...
            $datos_pagos= $this->pagoModelo->listar_anulados();

            $datos= array();

        
            foreach ($datos_pagos as $pagos) {
             $datos[]=array(//! guardamos los datos en el array creado con la siguiente estructura
                 "0"=>Encriptacion::encryption($pagos->idpago), //! indece 0 para el codigo
     
                 "1"=>$pagos->servicio,
                 "2"=>'$'. number_format((float)round($pagos->total_pagar,2),2,'.','') ,
                 "3"=>$pagos->mes,
                 "4"=>$pagos->fecha,
                 "5"=>$pagos->tarifa_admin>0?'$'.$pagos->tarifa_admin:"N/A",
                 //! en el indice 3 elvaluamo la condicion del registro si es activo osea 1 mostrara un label verde u la leyenta activo
                 //! si la condicion es inactivo osea 0 mostrara un label rojo con la leyenda inactivo
                 "6"=>$pagos->condicion==0?"<span class='label label-danger'>NULO</span>":
                 ""
                
              );
            }
          
            $resultado=array( //! array con la informacion para el DATATABLE DE JQUERY
                
             "sEcho"=>1, //!Informacion para el datatables
             "iTotalRecords"=>count($datos),//!Total de registros para el datatable
             "iTotalDisplayRecords"=>count($datos),//!Datos totales a visualizar
             "aaData"=>$datos //! los datos para el datatable seria el array de la consulta ya con el formato necesario
          );
     
            echo json_encode($resultado);
       }else {
            
       redireccionar();
        }
        
    }
}

}//fin ifpermiso
else{
    redireccionar();
}
}//fin if session nombre

ob_end_flush();