
<?php

ob_start();
session_start();

if (!isset($_SESSION['nombre'])) {
  redireccionar('login');
}else{

    if ($_SESSION['permiso']=="usuario") {
    
    
Class Pagos_pendientes extends Controlador{

    function __construct(){

        $this->cobroModelo=$this->modelo('Cobro');
    }

    public function index(){

       // echo date('Y-m-d');
        $this->vista('pagos/pagos_pendientes_vista');
    }

    public function listar(){

        if ($_SERVER['REQUEST_METHOD']=='POST') {
            # code...
            $usuario_cod=Encriptacion::decryption($_SESSION['idusuario']);
            $datos_cobros= $this->cobroModelo->listar_pendientes($usuario_cod);

            $datos= array();

            
            foreach ($datos_cobros as $cobros) {
             $datos[]=array(//! guardamos los datos en el array creado con la siguiente estructura
                 "0"=>Encriptacion::encryption($cobros->idcobro), //! indece 0 para el codigo
     
                 //! en el indice 2 guarda el nombre del registro
                 "1"=>$cobros->servicio,
                
                 "2"=>'$'. number_format((float)round($cobros->total_pagar,2),2,'.','') ,
                 "3"=>$cobros->mes,
                 "4"=>$cobros->fecha,
                 "5"=>$cobros->tipo_pago,
                 
              );
            }
        

        
            $resultado=array( //! array con la informacion para el DATATABLE DE JQUERY
                
             "sEcho"=>1, //!Informacion para el datatables
             "iTotalRecords"=>count($datos),//!Total de registros para el datatable
             "iTotalDisplayRecords"=>count($datos),//!Datos totales a visualizar
             "aaData"=>$datos //! los datos para el datatable seria el array de la consulta ya con el formato necesario
          );
     
            echo json_encode($resultado);
        }else {
            
           redireccionar();
        }
        
    }


    public function mostrar_total(){
        if ($_SERVER['REQUEST_METHOD']=='POST') {

            $codigo=Encriptacion::decryption($_SESSION['idusuario']);
            $deuda= $this->cobroModelo->mostrar_total_deuda($codigo);

            echo '$'. number_format((float)round($deuda->total,2),2,'.','');
            

        }else{

            redireccionar();
        }

    }

}

}//fin ifpermiso
else{
    redireccionar();
}
}//fin if session nombre

ob_end_flush();