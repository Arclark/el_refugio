
<?php

ob_start();
session_start();

if (!isset($_SESSION['nombre'])) {
  redireccionar('login');
}else{

    if ($_SESSION['permiso']=="usuario") {
    
    
Class Pagos_realizados extends Controlador{

    function __construct(){

        $this->cobroModelo=$this->modelo('Cobro');
    }

    public function index(){

       // echo date('Y-m-d');
        $this->vista('pagos/pagos_realizados_vista');
    }

    public function listar_trans(){

        if ($_SERVER['REQUEST_METHOD']=='POST') {
            # code...
            $usuario_cod=Encriptacion::decryption($_SESSION['idusuario']);
            $datos_trans= $this->cobroModelo->transaccion($usuario_cod);

            $datos= array();

            
            foreach ($datos_trans as $trans) {
             $datos[]=array(//! guardamos los datos en el array creado con la siguiente estructura
                 "0"=>Encriptacion::encryption($trans->idtransaccion), //! indece 0 para el codigo
     
                 //! en el indice 2 guarda el nombre del registro
                 "1"=>$trans->fecha_pago,
                
                 "2"=>'$'. number_format((float)round($trans->efectivo,2),2,'.','') ,
                 "3"=>'$'. number_format((float)round($trans->total,2),2,'.','') ,
                 "4"=>'$'.$trans->cambio,
                 "5"=>"<button type='button' data-toggle='modal' data-target='#detalle_modal' onclick=detalle('".Encriptacion::encryption($trans->idtransaccion)."') class='btn btn-info'>Detalles <i class='glyphicon glyphicon-eye-open'></i></button>"
                 
              );
            }
        

        
            $resultado=array( //! array con la informacion para el DATATABLE DE JQUERY
                
             "sEcho"=>1, //!Informacion para el datatables
             "iTotalRecords"=>count($datos),//!Total de registros para el datatable
             "iTotalDisplayRecords"=>count($datos),//!Datos totales a visualizar
             "aaData"=>$datos //! los datos para el datatable seria el array de la consulta ya con el formato necesario
          );
     
            echo json_encode($resultado);
        }else {
            
           redireccionar();
        }
        
    }

    

    public function listar_detalle($trans=null){

        if ($_SERVER['REQUEST_METHOD']=='POST') {
            # code...
            $idtrans=Encriptacion::decryption($trans);
            $datos_trans= $this->cobroModelo->detalle($idtrans);

            $datos= array();

            
            foreach ($datos_trans as $trans) {
             $datos[]=array(//! guardamos los datos en el array creado con la siguiente estructura
                 "0"=>Encriptacion::encryption($trans->idcobro), //! indece 0 para el codigo
     
                 //! en el indice 2 guarda el nombre del registro
                 "1"=>$trans->servicio,
                
                 "2"=>'$'. number_format((float)round($trans->total_pagar,2),2,'.','') ,
                 "3"=>$trans->mes,
                 "4"=>$trans->fecha,
                 "5"=>$trans->tipo_pago
              );
            }
        

        
            $resultado=array( //! array con la informacion para el DATATABLE DE JQUERY
                
             "sEcho"=>1, //!Informacion para el datatables
             "iTotalRecords"=>count($datos),//!Total de registros para el datatable
             "iTotalDisplayRecords"=>count($datos),//!Datos totales a visualizar
             "aaData"=>$datos //! los datos para el datatable seria el array de la consulta ya con el formato necesario
          );
     
            echo json_encode($resultado);
        }else {
            
           redireccionar();
        }
        
    }

}

}//fin ifpermiso
else{
    redireccionar();
}
}//fin if session nombre

ob_end_flush();