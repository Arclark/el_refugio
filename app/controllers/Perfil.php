<?php

ob_start();
session_start();

if (!isset($_SESSION['nombre'])) {
  redireccionar('login');
}else{

Class Perfil extends Controlador{

    public function __construct(){

        $this->perfilModelo=$this->modelo('Perfil_user');
        
       
    }

    public function index(){

        $id_user=Encriptacion::decryption($_SESSION['idusuario']);
        $datos=$this->perfilModelo->datos_user($id_user);

     $this->vista('perfil/perfil_vista',$datos);

    }

    public function cambiar_pass(){

        if ($_SERVER['REQUEST_METHOD']=="POST") {
        
        $pass_actual=$_POST['pass_actual'];
        $clavehash=hash("SHA256",$pass_actual.SALT);
        $new_pass=hash("SHA256",$_POST['pass_nueva'].SALT);
        
         $id_user=Encriptacion::decryption($_SESSION['idusuario']);

        $datos=$this->perfilModelo->verificar_pass($id_user,$clavehash);

        if ($datos->usuario==1) {
            $passnew=$this->perfilModelo->cambiar_pass($id_user,$new_pass);

            echo $passnew?"Contraseña fue cambiada con exito":$passnew;
        } else {
            echo 0;
        }
        
        } else {
            redireccionar('perfil');
        }
        
    }
   
}

}

ob_end_flush();

