<?php
ob_start();
session_start();

if (!isset($_SESSION['nombre'])) {
  redireccionar('login');
}else{

    if ($_SESSION['permiso']=="administrador") {
    
    

Class Personales extends Controlador{

    function __construct(){

       $this->pago_personalModelo=$this->modelo('Pago_personal');
    }

    public function index(){

       // echo date('Y-m-d');
        $this->vista('pagos_personales/pagos_personales_vista');
    }

    public function insert(){
        if ($_SERVER['REQUEST_METHOD']=='POST') {
            $mes=$_POST['mes'];
            $detalle=$_POST['detalle'];
            $total_pagar=$_POST['total_pagar'];
            $usuario=Encriptacion::decryption($_POST['usuario']);

            $mensaje=$this->pago_personalModelo->insert($usuario,$detalle,$total_pagar,$mes);

            echo $mensaje==1?"Pago regístrado con éxito":$mensaje;


        }else{

            redireccionar(personales);
        }
    }

    public function anular(){
        if ($_SERVER['REQUEST_METHOD']=='POST') {
           $codigo=Encriptacion::decryption($_POST['codigo']);

            $mensaje=$this->pago_personalModelo->anular($codigo);

            echo $mensaje==1?"Pago Anulado con éxito":$mensaje;


        }else{

            redireccionar(pagos);
        }
    }
    public function mostrar_usuario(){
        if ($_SERVER['REQUEST_METHOD']=='POST') {

            $datos_usuarios= $this->pago_personalModelo->mostrar_usuario();

            echo "<option value='' selected=''>--Opciones--</option>";
            foreach ($datos_usuarios as $usuarios) {
                # code...
                echo "<option value=" .Encriptacion::encryption($usuarios->idusuario) . ">" . $usuarios->nombre . "</option>";
 
          } 

        }else{

            redireccionar(personales);
        }

    }


    public function listar(){

        if ($_SERVER['REQUEST_METHOD']=='POST') {
            # code...
            $datos_pagos= $this->pago_personalModelo->listar();

            $datos= array();

        
            foreach ($datos_pagos as $pagos) {
             $datos[]=array(//! guardamos los datos en el array creado con la siguiente estructura
                 "0"=>Encriptacion::encryption($pagos->idpago_personal), //! indece 0 para el codigo
     
                 //! en el indice 1 se crean un condicional de una linea para crear los botones de Editar|Desactivar o Editar|Activar dependiendo de la condicion de los registros
                 "1"=>"<button class='btn btn-danger' title='Anular Pago' onclick=anular('".Encriptacion::encryption($pagos->idpago_personal)."')>Anular <i class='fa fa-close'></i></button>",
                 //! en el indice 2 guarda el nombre del registro
                 "2"=>$pagos->nombre,
                 "3"=>$pagos->detalle,
                 "4"=>'$'. number_format((float)round($pagos->total_pagar,2),2,'.','') ,
                 "5"=>$pagos->mes,
                 "6"=>$pagos->fecha,
              
                 //! en el indice 3 elvaluamo la condicion del registro si es activo osea 1 mostrara un label verde u la leyenta activo
                 //! si la condicion es inactivo osea 0 mostrara un label rojo con la leyenda inactivo
                 "7"=>$pagos->condicion?"<span class='label bg-green'>Activo</span>":
                 ""
                
              );
            }
          
            $resultado=array( //! array con la informacion para el DATATABLE DE JQUERY
                
             "sEcho"=>1, //!Informacion para el datatables
             "iTotalRecords"=>count($datos),//!Total de registros para el datatable
             "iTotalDisplayRecords"=>count($datos),//!Datos totales a visualizar
             "aaData"=>$datos //! los datos para el datatable seria el array de la consulta ya con el formato necesario
          );
     
            echo json_encode($resultado);
        }else {
            
            redireccionar(personales);
        }
        
    }




}

}//fin ifpermiso
else{
    redireccionar();
}
}//fin if session nombre

ob_end_flush();