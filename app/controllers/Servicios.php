<?php
ob_start();
session_start();

if (!isset($_SESSION['nombre'])) {
  redireccionar('login');
}else{

    if ($_SESSION['permiso']=="administrador") {
    
    
Class Servicios extends Controlador{

    public function __construct(){

        $this->servicioModelo=$this->modelo('Servicio');
       
    }

    public function index(){
    
        $servicios=$this->servicioModelo->listar();
        
     $this->vista('servicios/servicios_vista',$servicios);

    }

    public function insertaroreditar(){

        if ($_SERVER['REQUEST_METHOD']=="POST") {
          $codigo= Encriptacion::decryption($_POST['codigo']);
          $nombre=$_POST['nombre'];
          
          if (empty($codigo)) {
              $mensaje=$this->servicioModelo->insert($nombre);

              echo $mensaje?"Regístro guardado con éxito":"No se pudo guardar";
          }else{

                $mensaje=$this->servicioModelo->update($codigo,$nombre);
                echo $mensaje?"Regístro actualizado con éxito":"No se pudo actualizar";
          }

        } else {
            redireccionar(servicios);
        }
        


    }
    public function mostrar(){
        if ($_SERVER['REQUEST_METHOD']=="POST") {
            $codigo=Encriptacion::decryption($_POST['codigo']);
            $datos_servicio=$this->servicioModelo->mostrar($codigo);

            echo json_encode($datos_servicio);

        }else {
            redireccionar(servicios);
        }
    }

    public function listar(){

        if ($_SERVER['REQUEST_METHOD']=="POST") {
        $datos_servicios=$this->servicioModelo->listar();

        $datos= array();

        
       foreach ($datos_servicios as $servicios) {
        $datos[]=array(//! guardamos los datos en el array creado con la siguiente estructura
            "0"=>Encriptacion::encryption($servicios->idservicio), //! indece 0 para el codigo

            //! en el indice 1 se crean un condicional de una linea para crear los botones de Editar|Desactivar o Editar|Activar dependiendo de la condicion de los registros
            "1"=>$servicios->condicion?"<button class='btn btn-warning' title='Editar' onclick=mostrar('".Encriptacion::encryption($servicios->idservicio)."')><i class='fa fa-pencil'></i></button>".
            "<button class='btn btn-danger' title='Desactivar Regístro' onclick=desactivar('".Encriptacion::encryption($servicios->idservicio)."')><i class='fa fa-close'></i></button>":
            "<button class='btn btn-warning' title='Editar' onclick=mostrar('".Encriptacion::encryption($servicios->idservicio)."')><i class='fa fa-pencil'></i></button>".
            "<button class='btn btn-primary' title='Activar Regístro' onclick=activar('".Encriptacion::encryption($servicios->idservicio)."')><i class='fa fa-check'></i></button>
            ",
            //! en el indice 2 guarda el nombre del registro
            "2"=>$servicios->nombre,
            //! en el indice 3 elvaluamo la condicion del registro si es activo osea 1 mostrara un label verde u la leyenta activo
            //! si la condicion es inactivo osea 0 mostrara un label rojo con la leyenda inactivo
            "3"=>$servicios->condicion?"<span class='label bg-green'>Activo</span>":
            "<span class='label bg-red'>Inactivo</span>"
           
         );
       }
     
       $resultado=array( //! array con la informacion para el DATATABLE DE JQUERY
           
        "sEcho"=>1, //!Informacion para el datatables
        "iTotalRecords"=>count($datos),//!Total de registros para el datatable
        "iTotalDisplayRecords"=>count($datos),//!Datos totales a visualizar
        "aaData"=>$datos //! los datos para el datatable seria el array de la consulta ya con el formato necesario
     );

     echo json_encode($resultado); //! se hace un echo de el resultado en FORMATO JSON
    }else{

        redireccionar(servicios);
    }
    }


    public function activar($idservicio=null){
        if ($_SERVER['REQUEST_METHOD']=="POST") {
        $codigo=Encriptacion::decryption($idservicio);
        $mensaje=$this->servicioModelo->activar($codigo);

        echo $mensaje?"Regístro activado con exíto":"Regístro no se pudo de activar";
        
    }else{

        redireccionar(servicios);
    }

    }

    public function desactivar($idservicio=null){
        if ($_SERVER['REQUEST_METHOD']=="POST") {
        $codigo=Encriptacion::decryption($idservicio);
        $mensaje=$this->servicioModelo->desactivar($codigo);

        echo $mensaje?"Regístro desactivado con exíto":"Regístro no se pudo de desactivar";
        
    }else{

        redireccionar(servicios);
    }

    }
   
}
}//fin ifpermiso
else{
    redireccionar();
}
}//fin if session nombre

ob_end_flush();