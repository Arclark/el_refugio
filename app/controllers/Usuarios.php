
<?php
ob_start();
session_start();

if (!isset($_SESSION['nombre'])) {
  redireccionar('login');
}else{

    if ($_SESSION['permiso']=="administrador") {
    
    
Class Usuarios extends Controlador{

    function __construct(){

       $this->usuarioModelo=$this->modelo('Usuario');
       $this->perfilModelo=$this->modelo('Perfil_user');
    }

    public function index(){
      
        //echo Encriptacion::generar_password(10);
     $this->vista('usuarios/usuarios_vista');  
          
    }

    public function insertaroreditar(){

        if ($_SERVER['REQUEST_METHOD']=="POST") {
          $codigo= Encriptacion::decryption($_POST['codigo']);
          $nombre=$_POST['nombre'];
          $apellido=$_POST['apellido'];
          $telefono=$_POST['telefono'];
          $correo=$_POST['correo'];
          $tipo_user=isset($_POST['tipo_user'])?"visitante":"local";
          $permiso=isset($_POST['permiso'])?"administrador":"usuario";
          
          $clave=Encriptacion::generar_password(10);
          $clavehash=hash("SHA256",$clave.SALT);
          $mensaje_credenciales= Correo::bienvenida($correo,$clave);
          if (empty($codigo)) {
              
    $mensaje=$this->usuarioModelo->insert($nombre,$apellido,$telefono,$correo,$clavehash,$permiso,$tipo_user);
              Correo::Enviar_Email($correo,'Asignacion de Credenciales',$mensaje_credenciales);
              echo $mensaje==1?"Regístro guardado con éxito":$mensaje;
          }else{

                $mensaje=$this->usuarioModelo->update($codigo, $nombre,$apellido,$telefono,$correo,$permiso,$tipo_user);
                echo $mensaje==1?"Regístro actualizado con éxito":$mensaje;
          }

        } else {
            redireccionar(usuarios);
        }
        


    }
    public function mostrar(){
        if ($_SERVER['REQUEST_METHOD']=="POST") {
            $codigo=Encriptacion::decryption($_POST['codigo']);
            $datos_usuario=$this->usuarioModelo->mostrar($codigo);

            echo json_encode($datos_usuario);

        }else {
            redireccionar(usuarios);
        }
    }
    public function listar(){

        if ($_SERVER['REQUEST_METHOD']=="POST") {
        $datos_usuarios=$this->usuarioModelo->listar();

        $datos= array();

        
       foreach ($datos_usuarios as $usuarios) {
        $datos[]=array(//! guardamos los datos en el array creado con la siguiente estructura
            "0"=>Encriptacion::encryption($usuarios->idusuario), //! indece 0 para el codigo

            //! en el indice 1 se crean un condicional de una linea para crear los botones de Editar|Desactivar o Editar|Activar dependiendo de la condicion de los registros
            "1"=>$usuarios->condicion?"<button class='btn btn-warning' title='Editar' onclick=mostrar('".Encriptacion::encryption($usuarios->idusuario)."')><i class='fa fa-pencil'></i></button>".
            "<button class='btn btn-danger' title='Desactivar Regístro' onclick=desactivar('".Encriptacion::encryption($usuarios->idusuario)."')><i class='fa fa-close'></i></button>":
            "<button class='btn btn-warning' title='Editar' onclick=mostrar('".Encriptacion::encryption($usuarios->idusuario)."')><i class='fa fa-pencil'></i></button>".
            "<button class='btn btn-primary' title='Activar Regístro' onclick=activar('".Encriptacion::encryption($usuarios->idusuario)."')><i class='fa fa-check'></i></button>
            ",
            //! en el indice 2 guarda el nombre del registro
            "2"=>$usuarios->nombre,
            "3"=>$usuarios->telefono,
            "4"=>$usuarios->correo,
            "5"=>$usuarios->tipo_user=='local'?'<span class="label label-primary">LOCAL</span>':'<span class="label label-warning">VISITA</span>',
            //! en el indice 3 elvaluamo la condicion del registro si es activo osea 1 mostrara un label verde u la leyenta activo
            //! si la condicion es inactivo osea 0 mostrara un label rojo con la leyenda inactivo
            "6"=>$usuarios->condicion?"<span class='label bg-green'>Activo</span>":
            "<span class='label bg-red'>Inactivo</span>",
            "7"=>"<button type='button' data-toggle='modal' data-target='#detalle_modal' onclick=usuario_pass('".Encriptacion::encryption($usuarios->idusuario)."') class='btn btn-info'><i class='glyphicon glyphicon-wrench'></i>  Cambiar Contraseña </button>"
           
         );
       }
     
       $resultado=array( //! array con la informacion para el DATATABLE DE JQUERY
           
        "sEcho"=>1, //!Informacion para el datatables
        "iTotalRecords"=>count($datos),//!Total de registros para el datatable
        "iTotalDisplayRecords"=>count($datos),//!Datos totales a visualizar
        "aaData"=>$datos //! los datos para el datatable seria el array de la consulta ya con el formato necesario
     );

     echo json_encode($resultado); //! se hace un echo de el resultado en FORMATO JSON
    }else{

        redireccionar("usuarios");
    }
    }

    public function activar($idusuario=null){
        if ($_SERVER['REQUEST_METHOD']=="POST") {
        $codigo=Encriptacion::decryption($idusuario);
        $mensaje=$this->usuarioModelo->activar($codigo);

        echo $mensaje?"Regístro activado con exíto":"Regístro no se pudo de activar";
        
    }else{

        redireccionar(usuarios);
    }

    }

    public function desactivar($idusuario=null){
        if ($_SERVER['REQUEST_METHOD']=="POST") {
        $codigo=Encriptacion::decryption($idusuario);
        $mensaje=$this->usuarioModelo->desactivar($codigo);

        echo $mensaje?"Regístro desactivado con exíto":"Regístro no se pudo de desactivar";
        
    }else{

        redireccionar(usuarios);
    }

    }


    public function cambiar_pass(){

        if ($_SERVER['REQUEST_METHOD']=="POST") {
            $idusuario=$_POST['codigo'];
            $codigo=Encriptacion::decryption($idusuario);
            $pass=12345;
            $clavehash=hash("SHA256",$pass.SALT);
            $mensaje=$this->perfilModelo->cambiar_pass($codigo,$clavehash);
            
            echo $mensaje?"Contraseña restablecida con exito":$mensaje;
           
        }else{
    
            redireccionar(usuarios);
        }
    }
}
}//fin ifpermiso
else{
    redireccionar();
}
}//fin if session nombre

ob_end_flush();