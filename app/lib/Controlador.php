<?php

//Controlador Principal o global
class Controlador
{
    //Cargar Modelos
    public function modelo($modelo){
        require_once '../app/models/'.$modelo.'.php';

        //instaciar modelo

        return new $modelo;
    }

    
    //Cargar Vistas
    public function vista($vista,$datos=[]){
        //Verificar si vista existe

        if (file_exists('../app/views/'.$vista.'.php')) {
            require_once '../app/views/'.$vista.'.php';
        }else{

            die('La vista no existe');
        }

    }
}
