<?php

    /*
    Mapeo de la URL Ingresada en el navegador
    Ejemplo

    1-Controlador
    2-Método
    3-Posible parametro
    URL= /articulo/actualizar/4

    */

    class Core
    {
        protected $controladorActual='Inicio';
        protected $metodoActual='index';
        protected $parametros=[];

        //Constructor
        public function __construct(){
            //print_r($this->getUrl());
            $url= $this->getUrl();
            //Evaluar si el controlador Existe
            if (file_exists("../app/controllers/".ucwords($url[0]).".php")) {
               //si existe el con

               $this->controladorActual=ucwords($url[0]);

               unset($url[0]);
            }
            //Requerir controlador

            require_once '../app/controllers/'.$this->controladorActual.".php";
            $this->controladorActual= new $this->controladorActual();
       
            //Verificar si existen metodos
           if (isset($url[1])) {
            if (method_exists($this->controladorActual, $url[1])) {
                //Verificar metodos

                $this->metodoActual=$url[1];

                //Resetear indice
                unset($url[1]);

            }
         
           }

             //Obtener Parametro de la url

             $this->parametros= $url ? array_values($url) :[];

             //llamar callback con parametros array
 
             call_user_func_array([$this->controladorActual,$this->metodoActual],$this->parametros);
           
        }//Fin Constructor
        public function getUrl()
        {
            if (isset($_GET['url'])) {
                //Para cortar espacios en blanco hacia la derecga
                $url=rtrim($_GET['url'], '/');

                //FILTER_SANITIZE_URL Elimina todos los caracteres excepto letras, dígitos y $-_.+!*'(),{}|\\^~[]`<>#%";/?:@&=. 
                $url=filter_var($_GET['url'], FILTER_SANITIZE_URL);
                    
                $url=explode('/',$url);

                return $url;
           
            }
        }

        
    }
    
