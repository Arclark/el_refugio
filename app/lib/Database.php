<?php

class Database{

    private $host= DB_HOST;
    private $db= DB_NAME;
    private $user=DB_USER;
    private $password=DB_PASSWORD;
    private $port= PORT;

    private $pdo;
    private $stmt;
    private $error;

    public function __construct(){

        $dsn='mysql:host=' . $this->host.';dbname='.$this->db.';port='.$this->port;
        
        $opciones=array(
            PDO::ATTR_PERSISTENT => true,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        );

        //Crear una instancia de PDO

        try{
            $this->pdo= new PDO($dsn,$this->user,$this->password,$opciones);
            
            $this->pdo->exec('set names utf8');
        }
        catch(PDOException $e){

            $this->error=$e->getMessage();
            echo $this->error;
        }
    }

        public function consulta($sql){

            $this->stmt=$this->pdo->prepare($sql);
        }

        public function parametro($parametro,$valor,$tipo=null){

          if (is_null($tipo)) {
          
                switch(true){
                    case is_int($valor):
                       $tipo=PDO::PARAM_INT; 
                    break;

                    case is_bool($valor):
                       $tipo=PDO::PARAM_BOOL; 
                    break;

                    case is_null($valor):
                       $tipo=PDO::PARAM_NULL; 
                    break;

                    default:
                       $tipo=PDO::PARAM_STR; 
                    break;
                    
                }
          }
          $this->stmt->bindValue($parametro,$valor,$tipo);
        }
        //ejecutar consulta
        public function ejecutar_consulta(){
            return $this->stmt->execute();
        }
        //PAra varios Registros
        public function registros(){
            $this->ejecutar_consulta();
            return $this->stmt->fetchAll(PDO::FETCH_OBJ);
        }
        //Para un registro
        public function registro(){
            $this->ejecutar_consulta();
            return $this->stmt->fetch(PDO::FETCH_OBJ);
        }
        //retornar filas afectadas
        public function filas_afectadas(){
            return $this->stmt->rowCount();
        }

        public function id_insertado(){
            return $this->pdo->lastInsertId();
        }
        public function iniciar_transaccion(){

            return $this->pdo->beginTransaction();
        }


        public function finalizar_transaccion(){

            return $this->pdo->commit();
        }

        public function cancelar_transaccion(){

            return $this->pdo->rollBack();
        }


        
}