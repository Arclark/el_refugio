<?php

   
	define('METHOD','AES-256-CBC');
	define('SECRET_KEY','###Smarta_terminal__2018;UGB---By:AlumnosUGB');
	define('SECRET_IV','2018UGB');
	class Encriptacion{
		public static function encryption($palabra){
			$output=FALSE;
			$key=hash('sha256', SECRET_KEY);
			$iv=substr(hash('sha256', SECRET_IV), 0, 16);
			$output=openssl_encrypt($palabra, METHOD, $key, 0, $iv);
			$output=base64_encode($output);
			return $output;
		}
		public static function decryption($palabra){
			$key=hash('sha256', SECRET_KEY);
			$iv=substr(hash('sha256', SECRET_IV), 0, 16);
			$output=openssl_decrypt(base64_decode($palabra), METHOD, $key, 0, $iv);
			return $output;
		}
		
		public static function encryption_images($ruta_imagen){
			//! Nombre de la imagen
				$path = $ruta_imagen;
				
				//! Extensión de la imagen
				$type = pathinfo($path, PATHINFO_EXTENSION);
				
				//! Cargando la imagen
				$data = file_get_contents($path);
				
				//! Decodificando la imagen en base64
				$base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
				
				//! Mostrando la imagen
				//!echo '<img src="'.$base64.'"/>';
				
				// Mostrando el código base64
				return $base64;

		}


		public static function  generar_password($largo){
			$cadena_base =  'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
			$cadena_base .= '0123456789' ;
			$cadena_base .= '!@#%&*';
		   
			$password = '';
			$limite = strlen($cadena_base) - 1;
		   
			for ($i=0; $i < $largo; $i++)
			  $password .= $cadena_base[rand(0, $limite)];
		   
			return utf8_encode($password);
		  }
      
      
   }
	

	?>