<?php
      
               //incluir el archivo de la clase
               require_once RUTA_APP.'/Recursos/fpdf/fpdf.php';

              

           class FPDFV extends FPDF 
           { 
             
              function Header(){
               
               $this->Image(RUTA_URL.'Ruta de logo',5,5,25);
               $this->SetFont('Arial','B',15);
               $this->Cell(30);
               $this->Cell(128,10,$this->titulo_header,0,0,'C');

              $this->Ln(20);

              }

              function Footer(){
                date_default_timezone_set('America/El_Salvador');
               $this->SetY(-15);
               $this->SetFont('Arial','I',8);
               $this->Cell(0,10,utf8_decode('Página ').$this->PageNo().'/{nb}',0,0,'C');

               $this->Cell(-50,10,utf8_decode('Fecha impresión: ').date('d/m/Y g:i:s A'),0,0,'C');


              }
           }
           

           ?>