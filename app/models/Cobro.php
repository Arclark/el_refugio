<?php

Class Cobro{

    private $db;
    function __construct(){

        $this->db= new Database();

    }

    public function insert_transaccion($usuario,$efectivo,$total,$cambio){

        try{
          
            
            
            $this->db->consulta("INSERT INTO `transaccion`(`usuario`, `fecha_pago`, `efectivo`,`total`, `cambio`) VALUES ('$usuario',NOW(),'$efectivo','$total','$cambio')");
           
            $this->db->ejecutar_consulta();
             
           
            return $this->db->id_insertado();
            
        }catch(PDOException $e){
           
            return $e->getMessage();
        }

    }

    public function detalle_transaccion($transaccion,$cobro){

        try{
          
            $this->db->iniciar_transaccion();
            
            $this->db->consulta("INSERT INTO `detalle_transaccion` (`transaccion`, `cobro`) VALUES ('$transaccion','$cobro')");
           
            if ($this->db->ejecutar_consulta()) {
            $this->db->finalizar_transaccion();
            return $this->db->filas_afectadas();
            }
        }catch(PDOException $e){
            $this->db->cancelar_transaccion();
            return $e->getMessage();
        }

    }
    public function cobrar($idcobro){

        try{
            $this->db->iniciar_transaccion();
            $this->db->consulta("UPDATE `cobros` SET `estado`=1 WHERE idcobro='$idcobro'");
           
            if ($this->db->ejecutar_consulta()) {
                $this->db->finalizar_transaccion();
                return $this->db->filas_afectadas();
                }
            
        }catch(PDOException $e){
            $this->db->cancelar_transaccion();
            return $e->getMessage();
        }

    }
    

    public function listar($usuario){

        try{
            $this->db->iniciar_transaccion();
            $this->db->consulta("SELECT c.idcobro, c.servicio, c.total_pagar, m.mes, c.fecha, c.tipo_pago FROM cobros c INNER JOIN mes m on m.idmes=c.mes WHERE estado=0 AND condicion=1 AND usuario='$usuario' ");
            $this->db->finalizar_transaccion();            
            return $this->db->registros();
            
        }catch(PDOException $e){
            $this->db->cancelar_transaccion();
            return $e->getMessage();
        }

    }

    public function listar_pendientes($usuario){

        try{
            $this->db->iniciar_transaccion();
            $this->db->consulta("SELECT c.idcobro, c.servicio, c.total_pagar, m.mes, c.fecha, c.tipo_pago FROM cobros c INNER JOIN mes m on m.idmes=c.mes WHERE estado=0 AND condicion=1 AND usuario='$usuario' ");
            $this->db->finalizar_transaccion();            
            return $this->db->registros();
            
        }catch(PDOException $e){
            $this->db->cancelar_transaccion();
            return $e->getMessage();
        }

    }
    public function mostrar_total_deuda($id_usuario){

        try{
            $this->db->iniciar_transaccion();
            $this->db->consulta("SELECT SUM(total_pagar) as total FROM cobros WHERE usuario='$id_usuario' AND estado=0 and condicion=1 ");
            $this->db->finalizar_transaccion();            
            return $this->db->registro();
            
        }catch(PDOException $e){
            $this->db->cancelar_transaccion();
            return $e->getMessage();
        }

    }


    public function mostrar_pagos($id_cobro){

        try{
            $this->db->iniciar_transaccion();
            $this->db->consulta("SELECT ROUND(total_pagar,2) as total_pagar FROM cobros WHERE idcobro='$id_cobro'");
            $this->db->finalizar_transaccion();            
            return $this->db->registro();
            
        }catch(PDOException $e){
            $this->db->cancelar_transaccion();
            return $e->getMessage();
        }

    }
    public function mostrar_usuario(){

        try{
            $this->db->iniciar_transaccion();
            $this->db->consulta("SELECT `idusuario`, CONCAT(`nombre`,' ', `apellido`) as nombre, `tipo_user` FROM `usuario` WHERE condicion=1 AND permiso= 'usuario' ");
            $this->db->finalizar_transaccion();            
            return $this->db->registros();
            
        }catch(PDOException $e){
            $this->db->cancelar_transaccion();
            return $e->getMessage();
        }

    }


    public function transaccion($id_usuario){

        try{
            $this->db->iniciar_transaccion();
            $this->db->consulta("SELECT `idtransaccion`, `fecha_pago`, `efectivo`, `total`, `cambio` FROM `transaccion` WHERE usuario='$id_usuario'");
            $this->db->finalizar_transaccion();            
            return $this->db->registros();
            
        }catch(PDOException $e){
            $this->db->cancelar_transaccion();
            return $e->getMessage();
        }

    }

    public function detalle($id_trans){

        try{
            $this->db->iniciar_transaccion();
            $this->db->consulta("SELECT c.idcobro, c.servicio, c.total_pagar, m.mes, c.fecha, c.tipo_pago, c.condicion FROM detalle_transaccion t INNER JOIN cobros c ON c.idcobro=t.cobro INNER JOIN mes m on m.idmes=c.mes  WHERE t.transaccion='$id_trans' AND c.condicion=1");
            $this->db->finalizar_transaccion();            
            return $this->db->registros();
            
        }catch(PDOException $e){
            $this->db->cancelar_transaccion();
            return $e->getMessage();
        }

    }

   


    public function anular($idpago){

        try{
            $this->db->iniciar_transaccion();
            $this->db->consulta("UPDATE pagos SET condicion=0 WHERE idpago='$idpago'");
           
            if ($this->db->ejecutar_consulta()) {
                $this->db->finalizar_transaccion();
                return $this->db->filas_afectadas();
                }
            
        }catch(PDOException $e){
            $this->db->cancelar_transaccion();
            return $e->getMessage();
        }

    }

}