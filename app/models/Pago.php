<?php

Class Pago{

    private $db;
    function __construct(){

        $this->db= new Database();

    }

    public function insert($servicio,$total_pago,$mes,$tarifa_admin){

        try{
            $identicador=round(microtime(true));
            $this->db->iniciar_transaccion();
            if(empty($tarifa_admin)){
            $this->db->consulta("INSERT INTO `pagos`(`servicio`, `total_pagar`, `mes`, `fecha`,`identificador`, `condicion`) VALUES ('$servicio','$total_pago','$mes',CURDATE(),'PG$identicador',1)");
            
            }else{
                $this->db->consulta("INSERT INTO `pagos`(`servicio`, `total_pagar`, `mes`, `fecha`,`identificador`, `condicion`, `tarifa_admin`) VALUES ('$servicio','$total_pago','$mes',CURDATE(),'PG$identicador',1,'$tarifa_admin')");
            }
            if ($this->db->ejecutar_consulta()) {
            $this->db->finalizar_transaccion();
            return $this->db->filas_afectadas();
            }
        }catch(PDOException $e){
            $this->db->cancelar_transaccion();
            return $e->getMessage();
        }

    }

    

    public function listar(){

        try{
            $this->db->iniciar_transaccion();
            $this->db->consulta("SELECT p.idpago, s.nombre as servicio, p.total_pagar, m.mes, p.fecha, p.condicion, p.tarifa_admin FROM pagos p INNER JOIN servicios s ON p.servicio=s.idservicio INNER JOIN mes m ON m.idmes=p.mes WHERE p.condicion=1");
            $this->db->finalizar_transaccion();            
            return $this->db->registros();
            
        }catch(PDOException $e){
            $this->db->cancelar_transaccion();
            return $e->getMessage();
        }

    }

    public function listar_anulados(){

        try{
            $this->db->iniciar_transaccion();
            $this->db->consulta("SELECT p.idpago, s.nombre as servicio, p.total_pagar, m.mes, p.fecha, p.condicion, p.tarifa_admin FROM pagos p INNER JOIN servicios s ON p.servicio=s.idservicio INNER JOIN mes m ON m.idmes=p.mes WHERE p.condicion=0");
            $this->db->finalizar_transaccion();            
            return $this->db->registros();
            
        }catch(PDOException $e){
            $this->db->cancelar_transaccion();
            return $e->getMessage();
        }

    }
    public function mostrar($idpago){

        try{
            $this->db->iniciar_transaccion();
            $this->db->consulta("SELECT `idpago`, `servicio`, `total_pagar`, `mes`, `fecha`, `tarifa_admin` FROM `pagos` WHERE idpago='$idpago'");
            $this->db->finalizar_transaccion();            
            return $this->db->registro();
            
        }catch(PDOException $e){
            $this->db->cancelar_transaccion();
            return $e->getMessage();
        }

    }

    public function mostrar_servicios(){

        try{
            $this->db->iniciar_transaccion();
            $this->db->consulta("SELECT `idservicio`, `nombre` FROM `servicios` WHERE condicion=1");
            $this->db->finalizar_transaccion();            
            return $this->db->registros();
            
        }catch(PDOException $e){
            $this->db->cancelar_transaccion();
            return $e->getMessage();
        }

    }


   


    public function anular($idpago){

        try{
            $this->db->iniciar_transaccion();
            $this->db->consulta("UPDATE pagos SET condicion=0 WHERE idpago='$idpago'");
           
            if ($this->db->ejecutar_consulta()) {
                $this->db->finalizar_transaccion();
                return $this->db->filas_afectadas();
                }
            
        }catch(PDOException $e){
            $this->db->cancelar_transaccion();
            return $e->getMessage();
        }

    }

}