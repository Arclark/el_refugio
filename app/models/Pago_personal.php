<?php

Class Pago_personal{

    private $db;
    function __construct(){

        $this->db= new Database();

    }

    public function insert($usuario,$detalle,$total_pago,$mes){

        try{
            $identicador=round(microtime(true));
            $this->db->iniciar_transaccion();
           
            $this->db->consulta("INSERT INTO `pago_personal`(`usuario`,`detalle`, `total_pagar`, `mes`, `condicion`,`identificador`, `fecha`) VALUES ('$usuario','$detalle','$total_pago','$mes',1,'PP$identicador',CURDATE())");
            if ($this->db->ejecutar_consulta()) {
            $this->db->finalizar_transaccion();
            return $this->db->filas_afectadas();
            }
        }catch(PDOException $e){
            $this->db->cancelar_transaccion();
            return $e->getMessage();
        }

    }

    

    public function listar(){

        try{
            $this->db->iniciar_transaccion();
            $this->db->consulta("SELECT p.idpago_personal, CONCAT(u.nombre,' ',u.apellido) as nombre, p.detalle, p.total_pagar, m.mes, p.condicion, p.fecha FROM pago_personal p INNER JOIN usuario u ON u.idusuario= p.usuario INNER JOIN mes m ON m.idmes= p.mes WHERE p.condicion=1;");
            $this->db->finalizar_transaccion();            
            return $this->db->registros();
            
        }catch(PDOException $e){
            $this->db->cancelar_transaccion();
            return $e->getMessage();
        }

    }

    public function listar_por_usuario($id_usuario){

        try{
            $this->db->iniciar_transaccion();
            $this->db->consulta("SELECT p.idpago_personal, CONCAT(u.nombre,' ',u.apellido) as nombre, p.detalle, p.total_pagar, m.mes, p.condicion, p.fecha FROM pago_personal p INNER JOIN usuario u ON u.idusuario= p.usuario INNER JOIN mes m ON m.idmes= p.mes WHERE p.condicion=1 AND usuario='$id_usuario'");
            $this->db->finalizar_transaccion();            
            return $this->db->registros();
            
        }catch(PDOException $e){
            $this->db->cancelar_transaccion();
            return $e->getMessage();
        }

    }

   

    public function mostrar_usuario(){

        try{
            $this->db->iniciar_transaccion();
            $this->db->consulta("SELECT `idusuario`, CONCAT(`nombre`,' ', `apellido`) as nombre, `tipo_user` FROM `usuario` WHERE condicion=1 AND permiso= 'usuario' ");
            $this->db->finalizar_transaccion();            
            return $this->db->registros();
            
        }catch(PDOException $e){
            $this->db->cancelar_transaccion();
            return $e->getMessage();
        }

    }


   


    public function anular($idpago){

        try{
            $this->db->iniciar_transaccion();
            $this->db->consulta("UPDATE pago_personal SET condicion=0 WHERE idpago_personal='$idpago'");
           
            if ($this->db->ejecutar_consulta()) {
                $this->db->finalizar_transaccion();
                return $this->db->filas_afectadas();
                }
            
        }catch(PDOException $e){
            $this->db->cancelar_transaccion();
            return $e->getMessage();
        }

    }

}