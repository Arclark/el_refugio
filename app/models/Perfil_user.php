
<?php

Class Perfil_user {

    private $db;
    function __construct(){

        $this->db= new Database();

    }


    public function datos_user($id_user){

        try{
            $this->db->iniciar_transaccion();
            $this->db->consulta("SELECT `nombre`, `apellido`, `telefono`, `correo`, `permiso`, `tipo_user` FROM `usuario` WHERE idusuario='$id_user'");
            $this->db->finalizar_transaccion();            
            return $this->db->registro();
            
        }catch(PDOException $e){
            $this->db->cancelar_transaccion();
            return $e->getMessage();
        }

    }

    public function cambiar_pass($id_user,$pass){

        try{
            $this->db->iniciar_transaccion();
            $this->db->consulta("UPDATE `usuario` SET `password`='$pass' WHERE `idusuario`='$id_user'");
           if ($this->db->ejecutar_consulta()) {
                $this->db->finalizar_transaccion();
                return $this->db->filas_afectadas();
                }
            
        }catch(PDOException $e){
            $this->db->cancelar_transaccion();
            return $e->getMessage();
        }

    }

    public function verificar_pass($id_user,$pass){

        try{
            $this->db->iniciar_transaccion();
            $this->db->consulta("SELECT COUNT(*) as usuario FROM `usuario` WHERE idusuario='$id_user' AND password='$pass'");
      
            $this->db->finalizar_transaccion();
            return $this->db->registro();
                
            
        }catch(PDOException $e){
            $this->db->cancelar_transaccion();
            return $e->getMessage();
        }

    }
}