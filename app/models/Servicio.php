<?php

Class Servicio{

    private $db;
    function __construct(){

        $this->db= new Database();

    }

    public function insert($nombre){

        try{
            $this->db->iniciar_transaccion();
            $this->db->consulta("INSERT INTO servicios(`nombre`, `condicion`) VALUES ('$nombre',1)");

            if ($this->db->ejecutar_consulta()) {
            $this->db->finalizar_transaccion();
            return $this->db->filas_afectadas();
            }
        }catch(PDOException $e){
            $this->db->cancelar_transaccion();
            return $e->getMessage();
        }

    }

    public function update($idservicio,$nombre){

        try{
            $this->db->iniciar_transaccion();
            $this->db->consulta("UPDATE servicios SET nombre='$nombre' WHERE idservicio='$idservicio'");
           if ($this->db->ejecutar_consulta()) {
                $this->db->finalizar_transaccion();
                return $this->db->filas_afectadas();
                }
            
        }catch(PDOException $e){
            $this->db->cancelar_transaccion();
            return $e->getMessage();
        }

    }

    public function listar(){

        try{
            $this->db->iniciar_transaccion();
            $this->db->consulta("SELECT idservicio, nombre, condicion FROM servicios");
            $this->db->finalizar_transaccion();            
            return $this->db->registros();
            
        }catch(PDOException $e){
            $this->db->cancelar_transaccion();
            return $e->getMessage();
        }

    }

    public function mostrar($idservicio){

        try{
            $this->db->iniciar_transaccion();
            $this->db->consulta("SELECT nombre FROM servicios WHERE idservicio='$idservicio'");
            $this->db->finalizar_transaccion();            
            return $this->db->registro();
            
        }catch(PDOException $e){
            $this->db->cancelar_transaccion();
            return $e->getMessage();
        }

    }



    public function activar($idservicio){

        try{
            $this->db->iniciar_transaccion();
            $this->db->consulta("UPDATE servicios SET condicion=1 WHERE idservicio='$idservicio'");
           
            if ($this->db->ejecutar_consulta()) {
                $this->db->finalizar_transaccion();
                return $this->db->filas_afectadas();
                }
            
        }catch(PDOException $e){
            $this->db->cancelar_transaccion();
            return $e->getMessage();
        }

    }


    public function desactivar($idservicio){

        try{
            $this->db->iniciar_transaccion();
            $this->db->consulta("UPDATE servicios SET condicion=0 WHERE idservicio='$idservicio'");
           
            if ($this->db->ejecutar_consulta()) {
                $this->db->finalizar_transaccion();
                return $this->db->filas_afectadas();
                }
            
        }catch(PDOException $e){
            $this->db->cancelar_transaccion();
            return $e->getMessage();
        }

    }

}