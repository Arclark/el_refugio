<?php

Class Usuario{

    private $db;
    function __construct(){

        $this->db= new Database();

    }

    public function insert($nombre,$apellido,$telefono,$correo,$pass,$permiso,$tipo_user){

        try{
            $this->db->iniciar_transaccion();
            $this->db->consulta("INSERT INTO usuario(nombre, apellido, telefono, correo, password, condicion, permiso, tipo_user) VALUES ('$nombre','$apellido','$telefono','$correo','$pass',1,'$permiso','$tipo_user')");

            if ($this->db->ejecutar_consulta()) {
            $this->db->finalizar_transaccion();
            return $this->db->filas_afectadas();
            }
        }catch(PDOException $e){
            $this->db->cancelar_transaccion();
            return $e->getMessage();
        }

    }

    public function update($codigo, $name,$apellido,$telefono,$correo,$permiso,$tipo_user){

        try{
            $this->db->iniciar_transaccion();
            $this->db->consulta("UPDATE `usuario` SET `nombre`='$name',`apellido`='$apellido',`telefono`='$telefono',`correo`='$correo',`permiso`='$permiso',`tipo_user`='$tipo_user' WHERE `idusuario`=$codigo");
           if ($this->db->ejecutar_consulta()) {
                $this->db->finalizar_transaccion();
                return $this->db->filas_afectadas();
                }
            
        }catch(PDOException $e){
            $this->db->cancelar_transaccion();
            return $e->getMessage();
        }

    }

    public function listar(){

        try{
            $this->db->iniciar_transaccion();
            $this->db->consulta("SELECT idusuario, CONCAT(nombre,' ', apellido) as nombre, telefono, correo, condicion, permiso, tipo_user FROM usuario where permiso='usuario'");
            $this->db->finalizar_transaccion();            
            return $this->db->registros();
            
        }catch(PDOException $e){
            $this->db->cancelar_transaccion();
            return $e->getMessage();
        }

    }

    public function mostrar($idusuario){

        try{
            $this->db->iniciar_transaccion();
            $this->db->consulta("SELECT idusuario, nombre, apellido, telefono, correo, permiso, tipo_user FROM usuario where idusuario='$idusuario'");
            $this->db->finalizar_transaccion();            
            return $this->db->registro();
            
        }catch(PDOException $e){
            $this->db->cancelar_transaccion();
            return $e->getMessage();
        }

    }



    public function activar($idusuario){

        try{
            $this->db->iniciar_transaccion();
            $this->db->consulta("UPDATE usuario SET condicion=1 WHERE idusuario='$idusuario'");
           
            if ($this->db->ejecutar_consulta()) {
                $this->db->finalizar_transaccion();
                return $this->db->filas_afectadas();
                }
            
        }catch(PDOException $e){
            $this->db->cancelar_transaccion();
            return $e->getMessage();
        }

    }


    public function desactivar($idusuario){

        try{
            $this->db->iniciar_transaccion();
            $this->db->consulta("UPDATE usuario SET condicion=0 WHERE idusuario='$idusuario'");
           
            if ($this->db->ejecutar_consulta()) {
                $this->db->finalizar_transaccion();
                return $this->db->filas_afectadas();
                }
            
        }catch(PDOException $e){
            $this->db->cancelar_transaccion();
            return $e->getMessage();
        }

    }

}