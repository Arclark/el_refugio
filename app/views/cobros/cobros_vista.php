<?php
require_once RUTA_APP .'/views/inc/header.php';
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    

    <!-- Main content -->
    <section class="content container-fluid">


<div class="row">
	<div class="col-md-12">
		
			<div class="box box-danger">
            <div class="box-header with-border">
                <div class=" text-center" >
              <h3>Panel de cobros </h3>
              </div>
            </div>

         
           <!-- INCIO FORM -->
           <div id="formulario">
            <form method="POST" id="formulario_cobros">
              <div class="box-body">
                <div class="form-group">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12  form-group">
                <label for="">Seleccione Usuario: </label>
                    
                    <select name="usuario" id="usuario" class="form-control selectpicker" data-live-search="true">
                    
                    </select> 
                </div>
                
                    <div id="deuda" class=" col-lg-offset-10 col-md-offset-10 col-sm-offset-10 form-group"> 
                    
                    <label for="">Total deuda</label>
                    <br>
                    <h4><b>
                   <span style="color: red" id="total_deuda" ></span>
                   </b></h4>
                   
                  
               
                    </div>

                    <div id="listado" class="panel-body table-responsive col-lg-12 col-md-12 col-sm-12 col-xs-6">
           
           <table id="tablacobros" class="table table-bordered table-condensed table-striped table-hover">
           <thead>
           <th  class=" bg-danger">Codigo</th>
          
           <th class=" bg-danger">Detalle</th>
           <th class=" bg-danger">Total</th>
           <th class=" bg-danger">Mes</th>
           <th class=" bg-danger">Fecha</th>
           <th class=" bg-danger">Tipo de Pago</th>
           <th class=" bg-danger">Seccionar todo <input type='checkbox' class='form-check-input' name='select_todo' id='select_todo'> </th>
       
           </thead>
           <tbody>

                 

       


           
           </tbody>
           
           </table>
          
          </div>
             
 

                </div>
                </div>
              
            </form>
            </div>

 
 


<!-- Modal -->
<div id="cobrar" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Panel de cobros</h4>
      </div>
      <div class="modal-body">
    
      <form method="POST" id="form_cobrar">
              <div class="box-body">
                <div class="form-group">
                		
							
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 form-group">
           
           <label>Efectivo </label><b><span style="color: red" id="errorEfectivo"></span></b>

           <div class="input-group">
               <div class="input-group-addon">
                    $
               </div>
           <input class="form-control" id="efectivo" name="efectivo" type="number" step="0.01" placeholder="0,00" />
            </div>			
               
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 form-group">
           
           <label>Monto </label><b><span style="color: red" id="errorMonto"></span></b>

           <div class="input-group">
               <div class="input-group-addon">
                    $
               </div>
           <input class="form-control" id="monto" name="monto" type="number" step="0.01" placeholder="0,00" />
            </div>			
               
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 form-group">
           
           <label>Cambio </label><b><span style="color: red" id="errorMonto"></span></b>

           <div class="input-group">
               <div class="input-group-addon">
                    $
               </div>
           <input class="form-control" id="cambio" name="cambio" type="number" step="0.01" placeholder="0,00" />
            </div>			
               
        </div>

                </div>
                </div>
              <div class="box-footer">
              <center>
              <button type="button" title="Cobrar" onclick="cobrar()" id="btnCobrar" name="btnCobrar" class="btn btn-primary"><i class="glyphicon glyphicon-floppy-saved"></i>  Cobrar</button>
              <button type="button" title="Cancelar y regresar" id="btnCancelar" onclick="limpiar()"  data-dismiss="modal" class="btn btn-danger"><i class="glyphicon glyphicon-circle-arrow-left"></i>  Cancelar</button>
               </center>
             </div>
            </form>

      </div>
     
    </div>

  </div>
</div>
       

          <div class="box-footer">

                 <button type="button" onclick="generar_cobro()" title="Procesar" id="btnProcesar" name="btnProcesar" class="btn btn-primary"><i class="glyphicon glyphicon-floppy-saved"></i>  Procesar cobro</button>
                 <button type="button" title="Cancelar y regresar" id="btnFinalizar" class="btn btn-danger" onclick="cancelar()"><i class="glyphicon glyphicon-circle-arrow-left"></i>  Finalizar</button>
               
             </div>
         
          </div>

	</div>
    
</div>
       
          




 </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->



<?php
require_once RUTA_APP .'/views/inc/footer.php';
?>
<script src="<?php echo RUTA_URL; ?>/scripts/cobros/cobros.js"></script>