
  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      Version 1.0
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; <?php echo date('Y'); ?> <a href="#">Ariel Claros & El Refugio</a>.</strong> Todos los derechos reservados.
  </footer>


</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="<?php echo RUTA_URL;?>/estilos/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo RUTA_URL;?>/estilos/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo RUTA_URL;?>/estilos/dist/js/adminlte.min.js"></script>

  <!-- DATATABLES -->
  <script src="<?php echo RUTA_URL;?>/estilos/datatables/jquery.dataTables.min.js"></script>    
    <script src="<?php echo RUTA_URL;?>/estilos/datatables/dataTables.buttons.min.js"></script>
    <script src="<?php echo RUTA_URL;?>/estilos/datatables/buttons.html5.min.js"></script>
    <script src="<?php echo RUTA_URL;?>/estilos/datatables/buttons.colVis.min.js"></script>
    <script src="<?php echo RUTA_URL;?>/estilos/datatables/jszip.min.js"></script>
    <script src="<?php echo RUTA_URL;?>/estilos/datatables/pdfmake.min.js"></script>
    <script src="<?php echo RUTA_URL;?>/estilos/datatables/vfs_fonts.js"></script>
    <script src="<?php echo RUTA_URL;?>/estilos/sweetalert/sweetalert.min.js"></script> 
    <script src="<?php echo RUTA_URL;?>/estilos/bootstrap/dist/js/bootstrap-select.min.js" ></script>
    <script src="<?php echo RUTA_URL;?>/estilos/bootstrap_files/bootstrap-filestyle.min.js"></script>
    <script src="<?php echo RUTA_URL;?>/estilos/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo RUTA_URL;?>/estilos/fastclick/lib/fastclick.js"></script>

<script src="<?php echo RUTA_URL;?>/estilos/iCheck/icheck.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo RUTA_URL;?>/estilos/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>

<!-- Page Script -->
<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
</body>
</html>

