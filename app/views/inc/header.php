<?php
if (strlen(session_id()) < 1) 
  session_start();
  
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
 
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?php echo RUTA_URL;?>/estilos/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo RUTA_URL;?>/estilos/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo RUTA_URL;?>/estilos/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo RUTA_URL;?>/estilos/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
  <link rel="stylesheet" href="<?php echo RUTA_URL;?>/estilos/dist/css/skins/skin-red.min.css">
  <link rel="stylesheet" href="<?php echo RUTA_URL;?>/estilos/bootstrap/dist/css/bootstrap-select.min.css">

<!-- JavaScript -->
<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/alertify.min.js"></script>

<!-- CSS -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/alertify.min.css"/>
<!-- Default theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/themes/default.min.css"/>
<!-- Semantic UI theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/themes/semantic.min.css"/>
<!-- Bootstrap theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/themes/bootstrap.min.css"/>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <link rel="stylesheet" type="text/css" href="<?php echo RUTA_URL;?>/estilos/datatables/jquery.dataTables.min.css">    
    <link href="<?php echo RUTA_URL;?>/estilos/datatables/buttons.dataTables.min.css" rel="stylesheet"/>
    <link href="<?php echo RUTA_URL;?>/estilos/datatables/responsive.dataTables.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="<?php echo RUTA_URL;?>/estilos/css/file.css">
    <link rel="stylesheet" href="<?php echo RUTA_URL;?>/estilos/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <link rel="stylesheet" href="<?php echo RUTA_URL;?>/estilos/iCheck/flat/blue.css">
  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>


<body class="hold-transition skin-red sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">

    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>E</b>R</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b><?php echo NOMBRE_SITIO; ?></b></span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div id="menu_perfil" class="navbar-custom-menu">
        <ul class="nav navbar-nav">

          <!-- User Account Menu -->
          <li class="dropdown user user-menu">
            <!-- Menu Toggle Button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <!-- The user image in the navbar-->
              <img  src="<?php echo RUTA_URL; ?>/files/logo_user.png" class="user-image" alt="User Image">
              <!-- hidden-xs hides the username on small devices so only the image appears. -->
              <span class="hidden-xs"><?php echo $_SESSION['nombre'];  ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- The user image in the menu -->
              <li class="user-header">
                <img  src="<?php echo RUTA_URL; ?>/files/logo_user.png" class="img-circle" alt="User Image">

                <p>
                <?php echo $_SESSION['nombre'];  ?>
                  <small><?php 
                  
                 
                  echo strftime("%A %d de %B del %Y");
                  
                  ?></small>
                </p>
              </li>
              
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?php echo RUTA_URL;?>/perfil" class="btn btn-default btn-flat">Mi Perfil</a>
                </div>
                <div class="pull-right">
                  
                  <a href="<?php echo RUTA_URL;?>/login/salir" id="btn_salir" class="btn btn-default btn-flat">Cerrar Sesión</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div id="panel_perfil" class="pull-left image">
          <img class="img-rounded"  src="<?php echo RUTA_URL; ?>/files/logo_user.png" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $_SESSION['nombre'];  ?></p>
          
          <a href="#"><i class="fa fa-circle text-success"></i>En línea</a>
        </div>
      </div>

  
      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
      
      <li class="header">Menu Principal</li>
      
      <li id="li_inicio" ><a href="<?php echo RUTA_URL;?>"><i class="fa fa-desktop"></i> <span>Inicio</span></a></li>
        
      </ul>

      <?php if($_SESSION['permiso']=="administrador"){  ?>
        <div id="admin">
      
      <ul class="sidebar-menu" data-widget="tree">

  
        <!-- Optionally, you can add icons to the links -->
        
        <li id="li_cobros"><a href="<?php echo RUTA_URL;?>/cobros"><i class="fa  fa-money"></i> <span> Cobros</span></a></li>
        
        <li id="li_pagos_personales"><a href="<?php echo RUTA_URL;?>/personales"><i class="fa fa-credit-card"></i> <span> Pagos Personales</span></a></li>
        
        <!-- pagos-->
        <li id="li_pagos" ><a href="<?php echo RUTA_URL;?>/pagos"><i class="fa fa-credit-card"></i> <span> Pagos Generales</span></a></li>

        <!-- usuarios-->
        <li id="li_usuarios"><a href="<?php echo RUTA_URL;?>/usuarios"><i class="fa fa-user"></i> <span> Usuarios</span></a></li>

         <!-- Servicios-->
        <li id="li_servicios" ><a href="<?php echo RUTA_URL;?>/servicios"><i class="fa fa-file-text"></i> <span> Servicios</span></a></li>

        
           </ul>

        
     
        </div>

<?php } ?>


<?php if($_SESSION['permiso']=="usuario"){  ?>
        <div id="cliente">
      
      <ul class="sidebar-menu" data-widget="tree">

  
        <!-- Optionally, you can add icons to the links -->
        <li id="li_pagos_pendientes"><a href="<?php echo RUTA_URL;?>/pagos_pendientes"><i class="fa fa-credit-card"></i> <span> Pagos Pendientes</span></a></li>
        
        <li id="li_pagos_realizados"><a href="<?php echo RUTA_URL;?>/pagos_realizados"><i class="fa fa-credit-card"></i> <span> Pagos Realizados</span></a></li>
        
        <li id="li_pagos_mensuales"><a href="<?php echo RUTA_URL;?>/pagos_mensuales"><i class="fa  fa-money"></i> <span> Pagos Mensuales</span></a></li>
        
        <!-- usuarios-->
        <li id="li_pagos_anulados"><a href="<?php echo RUTA_URL;?>/pagos_anulados"><i class="fa fa-user"></i> <span> Pagos anulados</span></a></li>

     
        
           </ul>

        
     
        </div>
<?php } ?>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

  