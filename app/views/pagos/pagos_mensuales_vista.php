<?php
require_once RUTA_APP .'/views/inc/header.php';
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    

    <!-- Main content -->
    <section class="content container-fluid">


<div class="row">
	<div class="col-md-12">
		
			<div class="box box-danger">
            <div class="box-header with-border">
          
              <h3>Pagos Mensuales <i class="fa fa-plus-square"></i></h3>
              
            </div>

 
             
           <div id="listadoregistros" class="panel-body table-responsive">
           <Center>

           <h4><b>Pagos Generales</b></h4>
           
           </Center>
            <table id="tabla_generales" class="table table-bordered table-condensed table-striped table-hover">
            <thead>
            <th  class=" bg-danger">Codigo</th>
            <th class=" bg-danger">Servicio</th>
            <th class=" bg-danger">Total a pagar</th>
            <th class=" bg-danger">Mes</th>
            <th class=" bg-danger">fecha</th>
            <th class=" bg-danger">Tarifa</th>
           
            </thead>
            <tbody>

          
            </tbody>
            
            </table>
           
           </div>


          
            
          <div id="listadoregistros" class="panel-body table-responsive">
           <Center>
          
           <h4> <b>Pagos Personales</b></h4>
           
           </Center>
            <table id="tabla_personales" class="table table-bordered table-condensed table-striped table-hover">
            <thead>
            <th  class=" bg-danger">Codigo</th>
            <th class=" bg-danger">Servicio</th>
            <th class=" bg-danger">Total a pagar</th>
            <th class=" bg-danger">Mes</th>
            <th class=" bg-danger">fecha</th>
           
            </thead>
            <tbody>

          
            </tbody>
            
            </table>
           
           </div>
         
          </div>

	</div>
</div>
       
          




 </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->



<?php
require_once RUTA_APP .'/views/inc/footer.php';
?>
<script src="<?php echo RUTA_URL; ?>/scripts/pagos/mensuales.js"></script>