
<?php
require_once RUTA_APP .'/views/inc/header.php';
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    

    <!-- Main content -->
    <section class="content container-fluid">


<div class="row">
	<div class="col-md-12">
		
			<div class="box box-danger">
            <div class="box-header with-border">
          
              <h3>Pagos Pendientes <i class="fa fa-plus-square"></i>
              
              <div id="deuda" class=" col-lg-offset-10 col-md-offset-10 col-sm-offset-10 form-group"> 
                    <center>
                    <label for="">Total deuda</label>
                    <br>
                    <h4><b>
                   <span style="color: red" id="total_deuda" ></span>
                   </b></h4>
                   
                  </center>
               
                    </div>
              </h3>
              

              
            </div>

 
             
           <div id="listadoregistros" class="panel-body table-responsive">
           
           <table id="pendientes" class="table table-bordered table-condensed table-striped table-hover">
           <thead>
           <th  class=" bg-danger">Codigo</th>
          
           <th class=" bg-danger">Detalle</th>
           <th class=" bg-danger">Total</th>
           <th class=" bg-danger">Mes</th>
           <th class=" bg-danger">Fecha</th>
           <th class=" bg-danger">Tipo de Pago</th>
          
           </thead>
           <tbody>

                 

       


           
           </tbody>
           
           </table>
           
           </div>


          
            
          
         
          </div>

	</div>
</div>
       
          




 </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->



<?php
require_once RUTA_APP .'/views/inc/footer.php';
?>
<script src="<?php echo RUTA_URL; ?>/scripts/pagos/pendientes.js"></script>