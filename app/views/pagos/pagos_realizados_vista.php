<?php
require_once RUTA_APP .'/views/inc/header.php';
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    

    <!-- Main content -->
    <section class="content container-fluid">


<div class="row">
	<div class="col-md-12">
		
			<div class="box box-danger">
            <div class="box-header with-border">
          
              <h3>Pagos Realizados <i class="fa fa-plus-square"></i></h3>
              
            </div>
          
           <div id="listadoregistros" class="panel-body table-responsive">
           
            <table id="tabla_trans" class="table table-bordered table-condensed table-striped table-hover">
            <thead>
            <th  class=" bg-danger">Codigo</th>
            <th class=" bg-danger">Fecha de Pago</th>
            <th class=" bg-danger">Dinero en efectivo</th>
            <th class=" bg-danger">Total Pagado</th>
            <th class=" bg-danger">Cambio</th>
            <th class=" bg-danger">Detalle de pago</th>
            </thead>
            <tbody>

          
            </tbody>
            
            </table>
           
           </div>

  

<!-- Modal -->
<div class="modal fade" id="detalle_modal" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Detalle de pago</h4>
      </div>
      <div class="modal-body">
     
     <div class=" table-responsive">
           <table style="width: 100%" id="tabla_detalle" class="table table-bordered table-condensed table-striped table-hover">
           <thead>
           <th  class=" bg-danger">Codigo</th>
           <th class=" bg-danger">Detalle</th>
           <th class=" bg-danger">Total Pago</th>
           <th class=" bg-danger">Mes</th>
           <th class=" bg-danger">Fecha</th>
           <th class=" bg-danger">Tipo de Pago</th>
           </thead>
           <tbody>

         
           </tbody>
           
           </table>
        </div>  
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

  </div>
</div>
            
          
         
          </div>

	</div>
</div>
       
          




 </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->



<?php
require_once RUTA_APP .'/views/inc/footer.php';
?>
<script src="<?php echo RUTA_URL; ?>/scripts/pagos/realizados.js"></script>