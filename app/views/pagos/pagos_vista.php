<?php
require_once RUTA_APP .'/views/inc/header.php';
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    

    <!-- Main content -->
    <section class="content container-fluid">


<div class="row">
	<div class="col-md-12">
		
			<div class="box box-danger">
            <div class="box-header with-border">
          
              <h3>Registro de pagos <button id="btnNuevo" class="btn btn-success" onclick="mostrarform(true)" ><i class="fa fa-plus-square"></i>  Nuevo</button></h3>
              
            </div>

             
           <div id="listadoregistros" class="panel-body table-responsive">
           
            <table id="tabla_pagos" class="table table-bordered table-condensed table-striped table-hover">
            <thead>
            <th  class=" bg-danger">Codigo</th>
            <th class=" bg-danger">Opciones</th>
            <th class=" bg-danger">Servicio</th>
            <th class=" bg-danger">Total a pagar</th>
            <th class=" bg-danger">Mes</th>
            <th class=" bg-danger">fecha</th>
            <th class=" bg-danger">Tarifa</th>
            <th class="bg-danger">Estado</th>
            </thead>
            <tbody>

          
            </tbody>
            
            </table>
           
           </div>


           <!-- INCIO FORM -->
           <div id="formularioregistros">
            <form method="POST" id="formulario_pagos">
              <div class="box-body">
                <div class="form-group">

                <div class="form-group col-lg-2 col-md-2 col-sm-6 col-xs-12">
                   
                   <label>Mes</label>
                    <select name="mes" id="mes" class="form-control selectpicker" data-live-search="true" >
                     <option value="1">Enero</option>
                     <option value="2">Febrero</option>
                     <option value="3">Marzo</option>
                     <option value="4">Abril</option>
                     <option value="5">Mayo</option>
                     <option value="6">Junio</option>
                     <option value="7">Julio</option>
                     <option value="8">Agosto</option>
                     <option value="9">Septiembre</option>
                     <option value="10">Octubre</option>
                     <option value="11">Noviembre</option>
                     <option value="12">Diciembre</option>
                    </select>
                </div>
          		
                <div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
                     <input type="hidden" name="codigo" id="codigo"> 
                     <label>Servicio  </label><b><span style="color: red" id="errorServicio"></span></b>
                      <select name="servicio" id="servicio" class="form-control selectpicker" data-live-search="true" >
                       
                      </select>
                  </div>


				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group">
           
				    <label>Total a pagar de factura: </label><b><span style="color: red" id="errorTotalPago"></span></b>

                    <div class="input-group">
                        <div class="input-group-addon">
                             $
                        </div>
                    <input class="form-control" id="total_pagar" name="total_pagar" type="number" step="0.01" placeholder="0,00" />
                     </div>			
                        
                 </div>

                 

                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group">
                
                <label>Tarifa (*)Opcional: </label>

                <div class="input-group">
                    <div class="input-group-addon">
                            $
                    </div>
                <input class="form-control" id="tarifa" name="tarifa" type="number" step="0.01" placeholder="0,00" />
                    </div>			
                    
                </div>


         
                 
                </div>
                </div>
              <div class="box-footer">
              <button type="submit" title="Guardar" id="btnGuardar" name="btnGuardar" class="btn btn-primary"><i class="glyphicon glyphicon-floppy-saved"></i>  Guardar</button>
              <button type="button" title="Cancelar y regresar" id="btnCancelar" class="btn btn-danger" onclick="cancelarform()"><i class="glyphicon glyphicon-circle-arrow-left"></i>  Cancelar</button>
                  
             </div>
            </form>
            </div>
          
         
          </div>

	</div>
</div>
       
          




 </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->



<?php
require_once RUTA_APP .'/views/inc/footer.php';
?>
<script src="<?php echo RUTA_URL; ?>/scripts/pagos/pagos.js"></script>