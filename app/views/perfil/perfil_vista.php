<?php

require_once RUTA_APP .'/views/inc/header.php';
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    

    <!-- Main content -->
    <section class="content container-fluid">


<div class="row">
	<div class="col-md-12 container">
		
			<div class="box box-danger">
            <div class="box-header with-border">
              
            <div class="panel panel-danger">
            <div class="panel-heading">
              <h3 class="panel-title">Perfil de Usuario</h3>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-3 col-lg-3 col-sm-3 " align="center"> <img alt="User Pic" src=<?php  echo Encriptacion::encryption_images(RUTA_URL."/files/datosdeusuario.png"); ?> class="img-circle img-responsive"> 
                <br>
               
                </div>
                
            
                <div class=" col-md-9 col-lg-9 col-sm-3 "> 
                  <table class="table table-user-information">
                    <tbody>
                      <tr>
                        <td><b>Nombres:</b></td>
                        <td><?php echo $datos->nombre; ?></td>
                      </tr>
                      <tr>
                        <td><b>Apellidos:</b> </td>
                        <td><?php echo $datos->apellido; ?></td>
                      </tr>
                      <tr>
                        <td><b>Teléfono:</b></td>
                        <td><?php echo $datos->telefono; ?></td>
                      </tr>
                   
                         <tr>
                             <tr>
                        <td><b>Correo:</b></td>
                        <td><?php echo $datos->correo; ?></td>
                      </tr>
                        <tr>
                        <td><b>Tipo de Usuario:</b> </td>
                        <td><?php echo $datos->tipo_user; ?></td>
                      </tr>
                                           
                    </tbody>
                  </table>
                  
                  <center>
                  <button data-toggle="modal" data-target="#modal_pass_user"  class="btn btn-primary">Cambiar contraseña</button>
               </center>
                </div>
              </div>
            </div>
                 
            
          </div>
            
           
            </fieldset>              
   
 


<!-- Modal -->
<div id="modal_pass_user" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Cambio de contraseñas</h4>
      </div>
      <div class="modal-body">
    
      <form method="POST" id="form_pass">
              <div class="box-body">
                <div class="form-group">
                
						
						
					
					<div class="col-lg-12 col-xs-12 col-md-12 col-sm-12 form-group " >
						<label>Contraseña actual: </label><b><span style="color: red" id="error_pass_a"></span></b> 
            <input type="hidden" name="codigo" id="codigo"  value=<?php echo $_SESSION['idusuario']; ?> >
						<input  type="password" autocomplete="off" required name="pass_actual"  id="pass_actual" placeholder="Escriba contraseña Actual"  class="form-control">
					</div>
          <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12 form-group ">
						<label>Contraseña nueva:</label><b><span style="color: red"  id="error_pass_n"></span></b> 
          
						<input type="password" autocomplete="off" name="pass_nueva" required  id="pass_nueva" placeholder="Escriba nueva contraseña" class="form-control">
					</div>
          <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12 form-group ">
						<label>Confirmar contraseña:</label><b><span style="color: red"  id="error_pass_n"></span></b> 
            
						<input type="password" autocomplete="off" name="pass_confirm" required id="pass_confirm" placeholder="Confirme nueva contraseña"  class="form-control">
				<b><span id="error" style="color: red;"></span></b> <br>
        	</div>

                </div>
                </div>
              <div class="box-footer">
              <center>
              <button type="submit" title="Guardar"  id="btnGuardar" name="btnGuardar" class="btn btn-primary"><i class="glyphicon glyphicon-floppy-saved"></i>  Guardar</button>
              <button type="button" title="Cancelar y regresar" id="btnCancelar" onclick="limpiar()" data-dismiss="modal" class="btn btn-danger"><i class="glyphicon glyphicon-circle-arrow-left"></i>  Cancelar</button>
               </center>
             </div>
            </form>

      </div>
     
    </div>

  </div>
</div>
            
           
          </div>

	</div>
</div>
       
          <!-- 
             Aqui el con tenido
          -->




 </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php
require_once RUTA_APP .'/views/inc/footer.php';
?>

<script src="<?php echo RUTA_URL; ?>/scripts/perfil/perfil.js"></script>



