<?php
require_once RUTA_APP .'/views/inc/header.php';
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    

    <!-- Main content -->
    <section class="content container-fluid">


<div class="row">
	<div class="col-md-12">
		
			<div class="box box-danger">
            <div class="box-header with-border">
          
              <h3>Administración de Servicios  <button id="btnNuevo" class="btn btn-success" onclick="mostrarform(true)" ><i class="fa fa-plus-square"></i>  Nuevo</button></h3>
              
            </div>

             
           <div id="listadoregistros" class="panel-body table-responsive">
           
            <table id="tabla_servicios" class="table table-bordered table-condensed table-striped table-hover">
            <thead>
            <th  class=" bg-danger">Codigo</th>
            <th class=" bg-danger">Opciones</th>
            <th class=" bg-danger">Servicio</th>
            <th class="bg-danger">Estado</th>
            </thead>
            <tbody>

            <?php /* foreach($datos as $servicios) : ?>

              <tr>
              <td  ><?php echo $servicios->idservicio; ?></td>
              <td  ><?php echo $servicios->nombre; ?></td>
              <td><?php echo $servicios->condicion; ?></td>
              </tr>

            <?php endforeach;*/ ?> 
            </tbody>
            
            </table>
           
           </div>


           <!-- INCIO FORM -->
           <div id="formularioregistros">
            <form method="POST" id="formulario_servicios">
              <div class="box-body">
                <div class="form-group">
          		
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
           <input type="hidden" name="codigo" id="codigo">
					
						<label>Nombre: </label><b><span style="color: red" id="errorNombre"></span></b>
						<input type="text" name="nombre"  id="nombre" placeholder="Nombre" maxlength="45" class="form-control">
					</div>

                </div>
                </div>
              <div class="box-footer">
              <button type="submit" title="Guardar" id="btnGuardar" name="btnGuardar" class="btn btn-primary"><i class="glyphicon glyphicon-floppy-saved"></i>  Guardar</button>
              <button type="button" title="Cancelar y regresar" id="btnCancelar" class="btn btn-danger" onclick="cancelarform()"><i class="glyphicon glyphicon-circle-arrow-left"></i>  Cancelar</button>
                  
             </div>
            </form>
            </div>
          
         
          </div>

	</div>
</div>
       
          




 </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->



<?php
require_once RUTA_APP .'/views/inc/footer.php';
?>
<script src="<?php echo RUTA_URL; ?>/scripts/servicios/servicios.js"></script>