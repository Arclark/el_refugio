<?php
require_once RUTA_APP .'/views/inc/header.php';
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    

    <!-- Main content -->
    <section class="content container-fluid">


<div class="row">
	<div class="col-md-12">
		
			<div class="box box-danger">
            <div class="box-header with-border">
          
              <h3>Administración de Usuarios  <button id="btnNuevo" class="btn btn-success" onclick="mostrarform(true)" ><i class="fa fa-plus-square"></i>  Nuevo</button></h3>
              
            </div>

             
           <div id="listadoregistros" class="panel-body table-responsive">
           
            <table id="tabla_usuarios" class="table table-bordered table-condensed table-striped table-hover">
            <thead>
            <th  class=" bg-danger">Codigo</th>
            <th class=" bg-danger">Opciones</th>
            <th class=" bg-danger">Nombre</th>
            <th class=" bg-danger">Teléfono</th>
            <th class=" bg-danger">Correo</th>
            <th class=" bg-danger">Tipo</th>
            <th class="bg-danger">Estado</th>
            <th class="bg-danger">Opciones</th>
            </thead>
            <tbody>

           
            </tbody>
            
            </table>
           
           </div>



           <!-- INCIO FORM -->
           <div id="formularioregistros">
            <form method="POST" id="formulario_usuarios">
              <div class="box-body">
                <div class="form-group">
          		
					<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
           <input type="hidden" name="codigo" id="codigo">
					
						<label>Nombre: </label><b><span style="color: red" id="errorNombre"></span></b>
						<input type="text" name="nombre"  id="nombre" placeholder="Nombre" maxlength="45" class="form-control">
					</div>
          <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 form-group">
           			
						<label>Apellido: </label><b><span style="color: red" id="errorApellido"></span></b>
						<input type="text" name="apellido"  id="apellido" placeholder="Apellido" maxlength="45" class="form-control">
					</div>

          <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 form-group">
             <label>Teléfono: </label><b><span style="color: red" id="errorTelefono"></span></b>
             <input type="text" name="telefono"  id="telefono" placeholder="Teléfono" maxlength="45" class="form-control">
          </div>
          <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 form-group">
             <label>Correo: </label><b><span style="color: red" id="errorCorreo"></span></b>
             <input type="email" name="correo"  id="correo" placeholder="Correo Eléctronico" maxlength="45" class="form-control">
          </div>

          <div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
                      <label>Tipo Usuario</label> 
                      <div class="checkbox">
                              <label><input type="checkbox" name="tipo_user" id="tipo_user">Guardar como Visitante</label>
                            </div>
                  </div>

          <div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
                      <label>Opciones Administrador</label> 
                      <div class="checkbox">
                              <label><input type="checkbox" name="permiso" id="permiso">Guardar como Administrador</label>
                            </div>
                  </div>
                </div>
                </div>
              <div class="box-footer">
              <button type="submit" title="Guardar" id="btnGuardar" name="btnGuardar" class="btn btn-primary"><i class="glyphicon glyphicon-floppy-saved"></i>  Guardar</button>
              <button type="button" title="Cancelar y regresar" id="btnCancelar" class="btn btn-danger" onclick="cancelarform()"><i class="glyphicon glyphicon-circle-arrow-left"></i>  Cancelar</button>
                  
             </div>
            </form>
            </div>
          
         
          </div>

	</div>
</div>
       
          




 </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->



<?php
require_once RUTA_APP .'/views/inc/footer.php';
?>
<script src="<?php echo RUTA_URL; ?>/scripts/usuarios/usuarios.js"></script>