-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 19-07-2018 a las 18:42:46
-- Versión del servidor: 5.7.21-log
-- Versión de PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `el_refugio`
--
CREATE DATABASE IF NOT EXISTS `el_refugio` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `el_refugio`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cobros`
--

DROP TABLE IF EXISTS `cobros`;
CREATE TABLE `cobros` (
  `idcobro` int(11) NOT NULL,
  `identificador` varchar(50) DEFAULT NULL,
  `servicio` varchar(50) DEFAULT NULL,
  `total_pagar` decimal(12,3) NOT NULL,
  `estado` tinyint(4) NOT NULL DEFAULT '0',
  `usuario` int(11) DEFAULT NULL,
  `mes` int(11) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `tipo_pago` varchar(50) DEFAULT NULL,
  `condicion` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_transaccion`
--

DROP TABLE IF EXISTS `detalle_transaccion`;
CREATE TABLE `detalle_transaccion` (
  `iddetalle_transaccion` int(11) NOT NULL,
  `transaccion` int(11) NOT NULL,
  `cobro` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mes`
--

DROP TABLE IF EXISTS `mes`;
CREATE TABLE `mes` (
  `idmes` int(11) NOT NULL,
  `mes` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `mes`
--

INSERT INTO `mes` (`idmes`, `mes`) VALUES
(1, 'Enero'),
(2, 'Febrero'),
(3, 'Marzo'),
(4, 'Abril'),
(5, 'Mayo'),
(6, 'Junio'),
(7, 'Julio'),
(8, 'Agosto'),
(9, 'Septiembre'),
(10, 'Octubre'),
(11, 'Noviembre'),
(12, 'Diciembre');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagos`
--

DROP TABLE IF EXISTS `pagos`;
CREATE TABLE `pagos` (
  `idpago` int(11) NOT NULL,
  `servicio` int(11) NOT NULL,
  `total_pagar` decimal(12,3) NOT NULL,
  `mes` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `identificador` varchar(25) NOT NULL,
  `condicion` tinyint(4) NOT NULL DEFAULT '1',
  `tarifa_admin` decimal(12,2) DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Disparadores `pagos`
--
DROP TRIGGER IF EXISTS `anular`;
DELIMITER $$
CREATE TRIGGER `anular` AFTER UPDATE ON `pagos` FOR EACH ROW UPDATE cobros SET condicion=0 WHERE identificador=NEW.identificador
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `pagos`;
DELIMITER $$
CREATE TRIGGER `pagos` AFTER INSERT ON `pagos` FOR EACH ROW IF (NEW.tarifa_admin>0.00) THEN
    
    set @total=NEW.total_pagar-NEW.tarifa_admin;
  
    INSERT INTO cobros(servicio,total_pagar, usuario, tipo_pago, mes,fecha,identificador) SELECT (SELECT nombre FROM servicios WHERE idservicio=NEW.servicio),ROUND(@total/(SELECT count(*) FROM usuario WHERE tipo_user='local' and permiso='usuario'),3), idusuario, 'GENERAL',NEW.mes, NEW.fecha, NEW.identificador FROM usuario where permiso='usuario';
    
    ELSE
    
    INSERT INTO cobros(servicio,total_pagar, usuario, tipo_pago, mes, fecha, identificador) SELECT (SELECT nombre FROM servicios WHERE idservicio=NEW.servicio),ROUND(NEW.total_pagar/(SELECT count(*) FROM usuario WHERE tipo_user='local'),3), idusuario, 'GENERAL', NEW.mes, NEW.fecha, NEW.identificador FROM usuario where permiso='usuario';
    
    END IF
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pago_personal`
--

DROP TABLE IF EXISTS `pago_personal`;
CREATE TABLE `pago_personal` (
  `idpago_personal` int(11) NOT NULL,
  `usuario` int(11) DEFAULT NULL,
  `detalle` varchar(45) NOT NULL,
  `total_pagar` decimal(12,3) NOT NULL,
  `mes` int(11) NOT NULL,
  `condicion` tinyint(4) NOT NULL,
  `identificador` varchar(50) DEFAULT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Disparadores `pago_personal`
--
DROP TRIGGER IF EXISTS `anular_personal`;
DELIMITER $$
CREATE TRIGGER `anular_personal` AFTER UPDATE ON `pago_personal` FOR EACH ROW UPDATE cobros SET condicion=0 WHERE identificador=NEW.identificador
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `personal_insert`;
DELIMITER $$
CREATE TRIGGER `personal_insert` AFTER INSERT ON `pago_personal` FOR EACH ROW INSERT INTO cobros(servicio,total_pagar,usuario,tipo_pago,mes,fecha,identificador) VALUES (NEW.detalle,NEW.total_pagar, NEW.usuario,'PERSONAL',NEW.mes, NEW.fecha, NEW.identificador )
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicios`
--

DROP TABLE IF EXISTS `servicios`;
CREATE TABLE `servicios` (
  `idservicio` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `condicion` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `servicios`
--

INSERT INTO `servicios` (`idservicio`, `nombre`, `condicion`) VALUES
(1, 'Alquiler de Casa', 1),
(2, 'Agua Potable', 1),
(3, 'Internet', 1),
(4, 'Luz electrica', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `transaccion`
--

DROP TABLE IF EXISTS `transaccion`;
CREATE TABLE `transaccion` (
  `idtransaccion` int(11) NOT NULL,
  `usuario` int(11) NOT NULL,
  `fecha_pago` datetime NOT NULL,
  `efectivo` decimal(12,3) NOT NULL,
  `total` decimal(12,3) NOT NULL,
  `cambio` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario` (
  `idusuario` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `telefono` varchar(60) NOT NULL,
  `correo` varchar(100) NOT NULL,
  `password` varchar(64) NOT NULL,
  `condicion` tinyint(4) NOT NULL DEFAULT '1',
  `permiso` varchar(20) NOT NULL,
  `tipo_user` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`idusuario`, `nombre`, `apellido`, `telefono`, `correo`, `password`, `condicion`, `permiso`, `tipo_user`) VALUES
(11, 'Osmin Ariel', 'Lopez Claros', '74444297', 'arielopez229422@hotmail.com', 'b493858636edde2c2fb9ca5afb29399c1de0608dcf2867716353e33de6c05277', 1, 'administrador', 'local'),
(12, 'Osmin Ariel', 'Lopez Claros', '78774729', 'arielopez229422@gmail.com', 'b493858636edde2c2fb9ca5afb29399c1de0608dcf2867716353e33de6c05277', 1, 'usuario', 'local');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cobros`
--
ALTER TABLE `cobros`
  ADD PRIMARY KEY (`idcobro`),
  ADD KEY `fk_usuario_cobros_idx` (`usuario`);

--
-- Indices de la tabla `detalle_transaccion`
--
ALTER TABLE `detalle_transaccion`
  ADD PRIMARY KEY (`iddetalle_transaccion`);

--
-- Indices de la tabla `mes`
--
ALTER TABLE `mes`
  ADD PRIMARY KEY (`idmes`);

--
-- Indices de la tabla `pagos`
--
ALTER TABLE `pagos`
  ADD PRIMARY KEY (`idpago`),
  ADD KEY `fk_servicio_pagos_idx` (`servicio`);

--
-- Indices de la tabla `pago_personal`
--
ALTER TABLE `pago_personal`
  ADD PRIMARY KEY (`idpago_personal`);

--
-- Indices de la tabla `servicios`
--
ALTER TABLE `servicios`
  ADD PRIMARY KEY (`idservicio`);

--
-- Indices de la tabla `transaccion`
--
ALTER TABLE `transaccion`
  ADD PRIMARY KEY (`idtransaccion`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idusuario`),
  ADD KEY `fk_usuario_permiso_idx` (`permiso`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cobros`
--
ALTER TABLE `cobros`
  MODIFY `idcobro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- AUTO_INCREMENT de la tabla `detalle_transaccion`
--
ALTER TABLE `detalle_transaccion`
  MODIFY `iddetalle_transaccion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `pagos`
--
ALTER TABLE `pagos`
  MODIFY `idpago` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `pago_personal`
--
ALTER TABLE `pago_personal`
  MODIFY `idpago_personal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `servicios`
--
ALTER TABLE `servicios`
  MODIFY `idservicio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `transaccion`
--
ALTER TABLE `transaccion`
  MODIFY `idtransaccion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `idusuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cobros`
--
ALTER TABLE `cobros`
  ADD CONSTRAINT `fk_usuario_cobros` FOREIGN KEY (`usuario`) REFERENCES `usuario` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `pagos`
--
ALTER TABLE `pagos`
  ADD CONSTRAINT `fk_servicio_pagos` FOREIGN KEY (`servicio`) REFERENCES `servicios` (`idservicio`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
