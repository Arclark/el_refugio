var tabla;
var datos=[];
function init() {
    
    $('#listado').hide();
    $('#deuda').hide();
    $('#btnProcesar').hide();
    $('#btnFinalizar').hide();
	 mostrar_usuario();
    operacion();
     $("#select_todo").click(function () {
        $(".form-check-input").prop('checked', $(this).is(':checked'));
     })
	document.getElementById("li_cobros").className = "active"; //! asignar la clase active a nuestro LI del menu en esta pagina
    document.title= "El refugio|Cobros"; //! asignar un Titulo en esta pagina
    mostrar_deuda();
}


function listar(usuario) {
    
    tabla=$('#tablacobros').dataTable(
        {
            "aProcessing": true,//Activamos el procesamiento del datatables
            "aServerSide": true,//Paginación y filtrado realizados por el servidor
            dom: 'Bfrtip',//Definimos los elementos del control de tabla
            buttons: [		          
                        
                    ],

             "ajax": 
                {
                        url: 'cobros/listar/'+usuario, 
                        type : "POST", 
                        dataType : "json", 					
                        error: function(e){
                            console.log(e.responseText);	
                        }
                },
                'columnDefs': [
                    {
                        "targets": 0, //! para la columna 0 
                        "className": "text-center", //! para mostra centrada          
                        "visible": false, //! no es visible ya que es el ID de los registros
                             "searchable": false //! si se puede buscar por id aunque no se vea
                             
                        //"width": "4%"
                   },
                   {
                    "targets": [1,3,6], //! para la columna 1 y tres 
                         "className": "text-center" //! texto centrado
                         
                   // "width": "4%"
                         },					
                 ],
            
            "bDestroy": true,
            "iDisplayLength": 5,//Paginación
            "order": [[ 2, "desc" ]]//Ordenar (columna,orden)
        }).DataTable();
}



function mostrar_usuario() {

$.post("cobros/mostrar_usuario", function(r){ //!enviamos por post l valor del deparamento
	$("#usuario").html(r);
	$('#usuario').selectpicker('refresh');
	

});
}

function mostrar_deuda(){

    $('#usuario').on('change',function(){ //! cuando ocurra el evento change
       
            var valor= $(this).val(); //!valor obtiene el codigo del departamento ya que los municipios son listados por departamentos
        $('#listado').show(); 
        $('#deuda').show();
         $('#btnProcesar').show();
         $('#btnFinalizar').show(); 
         $("#select_todo").prop('checked',false);
         listar(valor);
         mostrar_total(valor);
        
       
       
            
    
        });
}
function actualizar_deudas(valor){
    listar(valor);
         mostrar_total(valor);
}
function mostrar_total(valor){

    $.post("cobros/mostrar_total",{codigo : valor}, function(r){ //!enviamos por post l valor del deparamento
    $("#total_deuda").text(r);
    
    

});

}

function generar_cobro(){
  
    $("#monto").attr("readonly",true);
    $("#cambio").attr("readonly",true);
        if( $('.form-check-input').is(':checked')==false) {
           
        alertify.error('Seleccione servicio para poder cobrar');

    }else{
    
        var pagos = document.getElementsByName('pago');
        
      for (let i = 0; i <pagos.length; i++) {
       
           
       
           if (pagos[i].checked===true) {
             
            datos[i]=pagos[i].value;  
        }
          
        

          
      }

      $.ajax({
        type: "POST",
        url: "cobros/generar_cobro",
        data: {'pago':(datos)},//capturo array     
        success: function(r){
         
              
            
          
          
           var dato= parseFloat(r);
             $('#monto').val(dato);
            $('#cobrar').modal('show');
          
      }
});
    
    
}
    
    



}
function cancelar(){

   
    $("#usuario").val("");
    $('#usuario').selectpicker('refresh');
    $('#listado').hide();
    $('#deuda').hide();
    $('#btnProcesar').hide();
    $('#btnFinalizar').hide();
    $("#select_todo").prop('checked',false);
    $("#total_deuda").text("");
}

function limpiar(){

   
    $("#cambio").val("");
    $("#efectivo").val("");
    datos=[];
}

function operacion() {
    $("#efectivo").on('keyup', function(){
        var value = $(this).val();
       var cambio= $(this).val()-$('#monto').val();
       if (cambio>=0) {
        $("#cambio").val(parseFloat(cambio).toFixed(2));  
       }
       

    }).keyup();
}

function cobrar(){
if ($("#efectivo").val()=="") {
    alertify.error("Ingrese cantidad de efectivo recibida");
}
else{
    var efectivo=parseFloat($("#efectivo").val());
    if (efectivo<($("#monto").val())) {
        alertify.error("¡La cantidad ingresada es inferior al monto a cobrar!");
    }else{
      $.ajax({
        type: "POST",
        url: "cobros/cobrar",
        data: {'pago':(datos),'usuario':($("#usuario").val()),'efectivo':($("#efectivo").val()),'cambio':$("#cambio").val(),'total':$("#monto").val()},//capturo array     
        success: function(r){
  
            tabla.ajax.reload(null,false);
            mostrar_total($("#usuario").val());
            $('#cobrar').modal('hide');
            limpiar();
            datos=[];
            alertify.success(r);

         
      }
});
    }
}
    
    
     
    
    



}
init();