var tabla;
function init() {
	$("#formulario_pagos").on("submit",function(e) 
	//! si existe el evento submit en el formulario apartir del boton guardar
	{
		guardar(e);	//! llamado a la funcion guardar y editar y pasamos como argumento la accion del boton
	})
    mostrarform(false);
   listar();
	 mostrar_usuario();
	 mostrar_mes();
	document.getElementById("li_pagos_personales").className = "active"; //! asignar la clase active a nuestro LI del menu en esta pagina
	document.title= "El refugio|Pagos Personales"; //! asignar un Titulo en esta pagina
}

function limpiar()
{	$("#usuario").val("");
	$('#usuario').selectpicker('refresh');
	
	$("#total_pagar").val(""); 
    $("#detalle").val(""); 

  mostrar_mes();
	
}

function mostrarform(estado) 
{
	limpiar(); 
	if (estado) 
	{
		$("#listadoregistros").hide(); 
		$("#formularioregistros").show();
		
		$("#btnNuevo").hide();
	}
	else 
	{
		$("#listadoregistros").show(); 
		$("#formularioregistros").hide(); 
		$("#btnNuevo").show(); 
	}
}


function cancelarform() 
{
	limpiar(); 
	mostrarform(false);
	$('#errorNombre').text("")
}

function mostrar_mes(){
	var fecha=new Date();
	$("#mes").val(fecha.getMonth()+1); //!asigna el valor vacio para el input codigo
	 $('#mes').selectpicker('refresh');
	 
	 
}

function listar() {
    
    tabla=$('#tabla_pagos').dataTable(
        {
            "aProcessing": true,//Activamos el procesamiento del datatables
            "aServerSide": true,//Paginación y filtrado realizados por el servidor
            dom: 'Bfrtip',//Definimos los elementos del control de tabla
            buttons: [		          
                        
                    ],

             "ajax": 
                {
                        url: 'personales/listar', 
                        type : "POST", 
                        dataType : "json", 					
                        error: function(e){
                            console.log(e.responseText);	
                        }
                },
                'columnDefs': [
                    {
                        "targets": 0, //! para la columna 0 
                        "className": "text-center", //! para mostra centrada          
                        "visible": false, //! no es visible ya que es el ID de los registros
                             "searchable": false //! si se puede buscar por id aunque no se vea
                             
                        //"width": "4%"
                   },
                   {
                    "targets": [3,6,1,7,5,4], //! para la columna 1 y tres 
                         "className": "text-center" //! texto centrado
                         
                   // "width": "4%"
                         },					
                 ],
            
            "bDestroy": true,
            "iDisplayLength": 10,//Paginación
            "order": [[ 6, "desc" ]]//Ordenar (columna,orden)
        }).DataTable();
}

function mostrar_usuario() {

$.post("personales/mostrar_usuario", function(r){ //!enviamos por post l valor del deparamento
	$("#usuario").html(r);
	$('#usuario').selectpicker('refresh');
	

});
}




function anular(id_pago)
{ 
	swal({ //! mensaje de confirmacion para el usuario
						  title: "¿Está Seguro de anular el pago?", //!texto del mensaje
						  
						  icon: "warning", //! icono del mensaje
						  buttons:["Cancelar","Aceptar"], //! botones a mostrar
						  dangerMode: true, //
						})
						.then((aceptar) => { 
						  if (aceptar) {//! Si presiona aceptar

								//! Se envia por metodo post el codigo de departamento a activar
						  		$.post("personales/anular",{codigo: id_pago}, function(e){
        					swal({
								  title: e, //! se imprime el msj enviado por el controlador
								  icon: "success", //!icono
								  button: "Aceptar", //! boton aceptar
								  timer: 2000 //! temporizador
								});
	           				 tabla.ajax.reload(null,false); //! recargamos la tabla
        						}); 
						  }
						});
}


function guardar(e)
{
	 if ($('#usuario').val()=="") {
		e.preventDefault();
		$('#errorUsuario').text("  ¡Seleccione usuario!");
		setTimeout(function(){
			$('#errorUsuario').text("");
		},3000); 
    }else if($('#detalle').val()=="") {
		e.preventDefault();
		$('#errorDetalle').text("  ¡Campo Obligatorio!");
		setTimeout(function(){
			$('#errorDetalle').text("");
		},3000);
    }else if($('#total_pagar').val()=="") {
		e.preventDefault();
		$('#errorTotalPago').text("  ¡Ingrese un cantidad!");
		setTimeout(function(){
			$('#errorTotalPago').text("");
		},3000);
    
    } 
    else{
	e.preventDefault(); //! No se activará la acción predeterminada del evento es decir el evento submit
	//$("#btnGuardar").prop("disabled",true);
	var formData = new FormData($("#formulario_pagos")[0]); //!obtenemos todos los datos de todos los elementos del formulario
	
	$.ajax({ //!Peticion AJAX
		url: "personales/insert", //! URL de nuestro controlador con la transaccion guardr o editar
	    type: "POST", //! metodo por el cual se envian lo datos al controlador
	    data: formData, //! Datos a enviar
	    contentType: false, //! se especifica falso para indicar a JQUERY no configuar ningun encabezado de tipo de contenido 
	    processData: false, //! se especific falso para que JQUERY no procese ni tranforme los datos enviados

	    success: function(datos) //! si todo tiene exito
	    {                    
             	
             swal({ //! SWAL es  una alerta de la libreria de JAVA SCRIPT SWEET-ALERT
               title: datos, //! titulo del mesaje que sera el mensaje de exito que retorna el controlador despues de la peticion AJAX
              
               icon: "success", //!Icono de exito de la libreria SWEET-ALERT
               button: "Aceptar" //! Texto del Boton 
               //timer: 2000 //! y un temporizador de dos segundo para que se cierre la alerta en caso de no clickear aceptar
             });          
	          mostrarform(false); //! llamamos la funcion mostrar form con argumento falso para ocultar el formulario 
				
				 tabla.ajax.reload(null,false);  //! Recargar tabla de datos y se coloca null y false en sus argumentos para permanecer en la misma paginacion
				
				}

	});
	limpiar(); //!limpiar los inputs
}
}
init();