var tabla;
function init() {
	
  
    listar();
    mostrar_total();
document.getElementById("li_pagos_pendientes").className = "active"; //! asignar la clase active a nuestro LI del menu en esta pagina
document.title= "El refugio|Pagos Pendientes"; //! asignar un Titulo en esta pagina
}




function listar() {
    
    tabla=$('#pendientes').dataTable(
        {
            "aProcessing": true,//Activamos el procesamiento del datatables
            "aServerSide": true,//Paginación y filtrado realizados por el servidor
            dom: 'Bfrtip',//Definimos los elementos del control de tabla
            buttons: [		          
                        
                    ],

             "ajax": 
                {
                        url: 'pagos_pendientes/listar', 
                        type : "POST", 
                        dataType : "json", 					
                        error: function(e){
                            console.log(e.responseText);	
                        }
                },
                'columnDefs': [
                    {
                        "targets": 0, //! para la columna 0 
                        "className": "text-center", //! para mostra centrada          
                        "visible": false, //! no es visible ya que es el ID de los registros
                             "searchable": false //! si se puede buscar por id aunque no se vea
                             
                        //"width": "4%"
                   },
                   {
                    "targets": [1,2,3,4,5], //! para la columna 1 y tres 
                         "className": "text-center" //! texto centrado
                         
                   // "width": "4%"
                         },					
                 ],
            
            "bDestroy": true,
            "iDisplayLength": 5,//Paginación
            "order": [[ 4, "desc" ]]//Ordenar (columna,orden)
        }).DataTable();
}

function mostrar_total(valor){

    $.post("pagos_pendientes/mostrar_total", function(r){ //!enviamos por post l valor del deparamento
    $("#total_deuda").text(r);
    
    

});

}
init();