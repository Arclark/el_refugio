var tabla;
function init() {
	
  
    

document.getElementById("li_pagos_realizados").className = "active"; //! asignar la clase active a nuestro LI del menu en esta pagina
document.title= "El refugio|Pagos Realizados"; //! asignar un Titulo en esta pagina
listar();


}


function listar() {
    
    tabla=$('#tabla_trans').dataTable(
        {
            "aProcessing": true,//Activamos el procesamiento del datatables
            "aServerSide": true,//Paginación y filtrado realizados por el servidor
            dom: 'Bfrtip',//Definimos los elementos del control de tabla
            buttons: [		          
                        
                    ],

             "ajax": 
                {
                        url: 'pagos_realizados/listar_trans', 
                        type : "POST", 
                        dataType : "json", 					
                        error: function(e){
                            console.log(e.responseText);	
                        }
                },
                'columnDefs': [
                    {
                        "targets": 0, //! para la columna 0 
                        "className": "text-center", //! para mostra centrada          
                        "visible": false, //! no es visible ya que es el ID de los registros
                             "searchable": false //! si se puede buscar por id aunque no se vea
                             
                        //"width": "4%"
                   },
                   {
                    "targets": [1,2,3,4,5], //! para la columna 1 y tres 
                         "className": "text-center" //! texto centrado
                         
                   // "width": "4%"
                         },					
                 ],
            
            "bDestroy": true,
            "iDisplayLength": 5,//Paginación
            "order": [[ 1, "desc" ]]//Ordenar (columna,orden)
        }).DataTable();
}



function detalle(trans) {
   

    tabla=$('#tabla_detalle').dataTable(
        {
            "aProcessing": true,//Activamos el procesamiento del datatables
            "aServerSide": true,//Paginación y filtrado realizados por el servidor
            dom: 'Bfrtip',//Definimos los elementos del control de tabla
            buttons: [		          
                        
                    ],

             "ajax": 
                {
                        url: 'pagos_realizados/listar_detalle/'+trans, 
                        type : "POST", 
                        dataType : "json", 					
                        error: function(e){
                            console.log(e.responseText);	
                        }
                },
                'columnDefs': [
                    {
                        "targets": 0, //! para la columna 0 
                        "className": "text-center", //! para mostra centrada          
                        "visible": false, //! no es visible ya que es el ID de los registros
                             "searchable": false //! si se puede buscar por id aunque no se vea
                             
                        //"width": "4%"
                   },
                   {
                    "targets": [1,2,3,4,5], //! para la columna 1 y tres 
                         "className": "text-center" //! texto centrado
                         
                   // "width": "4%"
                         },					
                 ],
            
            "bDestroy": true,
            "iDisplayLength": 5,//Paginación
            "order": [[ 4, "desc" ]]//Ordenar (columna,orden)
        }).DataTable();

}


init();