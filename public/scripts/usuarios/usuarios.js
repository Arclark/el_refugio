var tabla;
function init() {
	$("#formulario_usuarios").on("submit",function(e) 
	//! si existe el evento submit en el formulario apartir del boton guardar
	{
		guardaryeditar(e);	//! llamado a la funcion guardar y editar y pasamos como argumento la accion del boton
	})
    mostrarform(false);
    listar();
 	
	document.getElementById("li_usuarios").className = "active"; //! asignar la clase active a nuestro LI del menu en esta pagina
	document.title= "El refugio|Usuarios"; //! asignar un Titulo en esta pagina
}

function limpiar()
{
	$("#codigo").val(""); 
    $("#nombre").val(""); 
    $("#apellido").val(""); 
    $("#telefono").val(""); 
    $("#correo").val(""); 
    $("#permiso").prop('checked',false);
    $("#tipo_user").prop('checked',false);
	
}

function mostrarform(estado) 
{
	limpiar(); 
	if (estado) 
	{
		$("#listadoregistros").hide(); 
		$("#formularioregistros").show();
		
		$("#btnNuevo").hide();
	}
	else 
	{
		$("#listadoregistros").show(); 
		$("#formularioregistros").hide(); 
		$("#btnNuevo").show(); 
	}
}


function cancelarform() 
{
	limpiar(); 
	mostrarform(false);
	$('#errorNombre').text("")
}
function mostrar(id_usuario)
{	//! por metodo post enviamos en datos de codigo con el id que recibimos
	$.post("usuarios/mostrar",{codigo : id_usuario}, function(data, status)
	{
		data = JSON.parse(data); //! El método JSON.parse() recibe una cadena JSON y en base a esta cadena construye un objeto JavaScript.		
		mostrarform(true); //! mostramos el formulario

		$("#nombre").val(data.nombre); //! Pasamos el valor a nuestros inputs
		$("#codigo").val(id_usuario);
        $("#apellido").val(data.apellido);
        $("#telefono").val(data.telefono);
        $("#correo").val(data.correo);

        if (data.permiso=="administrador") {
            $("#permiso").prop('checked',true);
        }

        if (data.tipo_user=="visitante") {
            $("#tipo_user").prop('checked',true);
        }
 	})
}

function listar() {
    
    tabla=$('#tabla_usuarios').dataTable(
        {
            "aProcessing": true,//Activamos el procesamiento del datatables
            "aServerSide": true,//Paginación y filtrado realizados por el servidor
            dom: 'Bfrtip',//Definimos los elementos del control de tabla
            buttons: [		          
                        
                    ],

             "ajax": 
                {
                        url: 'usuarios/listar', 
                        type : "POST", 
                        dataType : "json", 					
                        error: function(e){
                            console.log(e.responseText);	
                        }
                },
                'columnDefs': [
                    {
                        "targets": 0, //! para la columna 0 
                        "className": "text-center", //! para mostra centrada          
                        "visible": false, //! no es visible ya que es el ID de los registros
                             "searchable": false //! si se puede buscar por id aunque no se vea
                             
                        //"width": "4%"
                   },
                   {
                    "targets": [1,3], //! para la columna 1 y tres 
                         "className": "text-center" //! texto centrado
                         
                   // "width": "4%"
                         },					
                 ],
            
            "bDestroy": true,
            "iDisplayLength": 5,//Paginación
            "order": [[ 2, "desc" ]]//Ordenar (columna,orden)
        }).DataTable();
}

function prueba() {
    tabla.ajax.reload();
}


function activar(id_usuario)
{ 
	swal({ //! mensaje de confirmacion para el usuario
						  title: "¿Está Seguro de activar el registro?", //!texto del mensaje
						  
						  icon: "warning", //! icono del mensaje
						  buttons:["Cancelar","Aceptar"], //! botones a mostrar
						  dangerMode: true, //
						})
						.then((aceptar) => { 
						  if (aceptar) {//! Si presiona aceptar

								//! Se envia por metodo post el codigo de departamento a activar
						  		$.post("usuarios/activar/"+id_usuario, function(e){
        					swal({
								  title: e, //! se imprime el msj enviado por el controlador
								  icon: "success", //!icono
								  button: "Aceptar", //! boton aceptar
								  timer: 2000 //! temporizador
								});
	           				 tabla.ajax.reload(null,false); //! recargamos la tabla
        						}); 
						  }
						});
}

function desactivar(id_usuario)
{ 
	swal({ //! mensaje de confirmacion para el usuario
						  title: "¿Está Seguro de desactivar el registro?", //!texto del mensaje
						  
						  icon: "warning", //! icono del mensaje
						  buttons:["Cancelar","Aceptar"], //! botones a mostrar
						  dangerMode: true, //
						})
						.then((aceptar) => { 
						  if (aceptar) {//! Si presiona aceptar

								//! Se envia por metodo post el codigo de departamento a activar
						  		$.post("usuarios/desactivar/"+id_usuario, function(e){
        					swal({
								  title: e, //! se imprime el msj enviado por el controlador
								  icon: "success", //!icono
								  button: "Aceptar", //! boton aceptar
								  timer: 2000 //! temporizador
								});
	           				 tabla.ajax.reload(null,false); //! recargamos la tabla
        						}); 
						  }
						});
}


function guardaryeditar(e)
{
	if ($('#nombre').val()=="") {
		e.preventDefault();
		$('#errorNombre').text("  ¡Campo Requerido!");
		setTimeout(function(){
			$('#errorNombre').text("");
		},3000);
    }else if($('#apellido').val()=="") {
		e.preventDefault();
		$('#errorApellido').text("  ¡Campo Requerido!");
		setTimeout(function(){
			$('#errorApellido').text("");
		},3000);
    
    }else if($('#telefono').val()=="") {
		e.preventDefault();
		$('#errorTelefono').text("  ¡Campo Requerido!");
		setTimeout(function(){
			$('#errorTelefono').text("");
        },3000);
    }else if
    ($('#correo').val()=="") {
		e.preventDefault();
		$('#errorCorreo').text("  ¡Campo Requerido!");
		setTimeout(function(){
			$('#errorCorreo').text("");
        },3000);
    }
    else{
	e.preventDefault(); //! No se activará la acción predeterminada del evento es decir el evento submit
	//$("#btnGuardar").prop("disabled",true);
	var formData = new FormData($("#formulario_usuarios")[0]); //!obtenemos todos los datos de todos los elementos del formulario
	
	$.ajax({ //!Peticion AJAX
		url: "usuarios/insertaroreditar", //! URL de nuestro controlador con la transaccion guardr o editar
	    type: "POST", //! metodo por el cual se envian lo datos al controlador
	    data: formData, //! Datos a enviar
	    contentType: false, //! se especifica falso para indicar a JQUERY no configuar ningun encabezado de tipo de contenido 
	    processData: false, //! se especific falso para que JQUERY no procese ni tranforme los datos enviados

	    success: function(datos) //! si todo tiene exito
	    {                    
             	
             swal({ //! SWAL es  una alerta de la libreria de JAVA SCRIPT SWEET-ALERT
               title: datos, //! titulo del mesaje que sera el mensaje de exito que retorna el controlador despues de la peticion AJAX
              
               icon: "success", //!Icono de exito de la libreria SWEET-ALERT
               button: "Aceptar" //! Texto del Boton 
               //timer: 2000 //! y un temporizador de dos segundo para que se cierre la alerta en caso de no clickear aceptar
             });          
	          mostrarform(false); //! llamamos la funcion mostrar form con argumento falso para ocultar el formulario 
				
				 tabla.ajax.reload(null,false);  //! Recargar tabla de datos y se coloca null y false en sus argumentos para permanecer en la misma paginacion
				
				}

	});
	limpiar(); //!limpiar los inputs
}
}



function usuario_pass(iduser) {

	swal({ //! mesaje de alerta para prguntar al usuario
		title: "¿Desea Restablecer la contraseñ de este usuario?", //! texto del mensaje
		 icon: "info", //! icono del mensaje
		buttons:["Cancelar","Aceptar"], //! Botones a mostrar
		dangerMode: true,
	  })
	  .then((aceptar) => {
		if (aceptar) { //! si presiona aceptar

			//! Enviamos por metodo post el codigo que recibimos
		$.post("usuarios/cambiar_pass",{codigo : iduser}, function(e){
		  
			alertify.success(e);

		  
			  
});
}
});
}
init();